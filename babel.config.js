module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        extensions: ['.tsx'],
        alias: {
          '@actions': './src/actions',
          '@components': './src/components',
          '@containers': './src/containers',
          '@reducers': './src/reducers',
          '@graphql': './src/graphql',
          '@utils': './src/utils',
          '@icons': './assets/icons',
          '@images': './assets/images',
          '@interfaces': './src/interfaces',
        },
      },
    ],
  ],
};
