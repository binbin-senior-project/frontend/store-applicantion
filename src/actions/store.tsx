import {IStore} from '@interfaces/store';
import {client} from '@graphql/config';
import {GET_STORES, GET_STORE} from '@graphql/query';

export const SET_CURRENT_STORE = 'SET_CURRENT_STORE';
export const RESET_CURRENT_STORE = 'RESET_CURRENT_STORE';

export const loadCurrentStoreProfileAction = () => async (
  dispatch: Function,
  getState: any,
) => {
  
  const { auth } = getState();
  const { accessToken } = auth;

  const { data } = await client.query({
    fetchPolicy: 'no-cache',
    variables: { offset: 0, limit: 1 },
    query: GET_STORES,
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });

  if (data?.getStores) {

    const s = data?.getStores[0];

    const store: IStore = {
      id: s.id,
      logo: s.logo,
      name: s.name,
      tagline: s.tagline,
      phone: s.phone,
      latitude: s.latitude,
      longitude: s.longitude,
      category: s.category,
    }

    dispatch({
      type: SET_CURRENT_STORE,
      store,
    });

    return;
  }
};

export const setCurrentStoreProfileAction = () => async (
  dispatch: Function,
  getState: any,
) => {
  
  const { auth, store } = getState();
  const { accessToken } = auth;

  const { data } = await client.query({
    fetchPolicy: 'no-cache',
    variables: { storeID: store.id },
    query: GET_STORE,
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });

  if (data?.getStore) {

    const s = data?.getStore;

    const store: IStore = {
      id: s.id,
      logo: s.logo,
      name: s.name,
      tagline: s.tagline,
      phone: s.phone,
      latitude: s.latitude,
      longitude: s.longitude,
      category: s.category,
    }

    dispatch({
      type: SET_CURRENT_STORE,
      store,
    });

    return;
  }
};

export const resetCurrentStoreProfileAction = () => async (dispatch: Function) => {
  dispatch({type: RESET_CURRENT_STORE});
};
