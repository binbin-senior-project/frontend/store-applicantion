import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import {client} from '@graphql/config';
import {
  resetCurrentStoreProfileAction,
  loadCurrentStoreProfileAction,
} from './store';
import {SET_TOKEN_MESSAGING} from '@graphql/mutation';

export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'USER_REQUEST_LOGIN_SUCCESS';
export const USER_LOGIN_ERROR = 'USER_LOGIN_ERROR';
export const USER_LOGOUT = 'USER_LOGOUT';

export const setTokenAction = (accessToken: string) => async (
  dispatch: Function,
) => {
  return dispatch({
    type: USER_LOGIN_SUCCESS,
    data: {accessToken},
  });
};

export const setMessagingTokenAction = (
  accessToken: string,
  token: string,
) => async (dispatch: Function) => {
  await client.mutate({
    variables: {input: {token}},
    mutation: SET_TOKEN_MESSAGING,
    context: {headers: {authorization: `Bearer ${accessToken}`}},
  });

  return;
};

export const loginAction = (email: string, password: string) => async (
  dispatch: Function,
) => {
  await auth().signInWithEmailAndPassword(email, password);
  const user = await auth().currentUser;

  if (user) {
    const accessToken = (await user.getIdToken()).toString();
    const messagingToken = (await messaging().getToken()).toString();

    setTokenAction(accessToken)(dispatch);
    setMessagingTokenAction(accessToken, messagingToken)(dispatch);

    return;
  }

  dispatch({
    type: USER_LOGIN_ERROR,
    error: {message: 'Something wrong!'},
  });
};

export const logoutAction = () => async (dispatch: Function) => {
  auth().signOut();

  resetCurrentStoreProfileAction()(dispatch);

  return dispatch({type: USER_LOGOUT});
};
