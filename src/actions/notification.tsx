export const SET_NEW_NOTIFICATION = 'SET_NEW_NOTIFICATION';
export const RESET_NOTIFICATION = 'RESET_NOTIFICATION';

export const setNewNotifcationAction = () => (dispatch: Function) => {
  dispatch({type: SET_NEW_NOTIFICATION});
};

export const resetNotificationAction = () => (dispatch: Function) => {
  dispatch({type: RESET_NOTIFICATION});
};
