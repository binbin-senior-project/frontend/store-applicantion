import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import {client} from '@graphql/config';
import {REQUEST_CREATE_STORE} from '@graphql/mutation';
import {setTokenAction, setMessagingTokenAction} from '@actions/auth';

export const REGISTER_RESET = 'REGISTER_RESET';
export const REGISTER_SET_EMAIL = 'REGISTER_SET_EMAIL';
export const REGISTER_SET_PASSWORD = 'REGISTER_SET_PASSWORD';
export const REGISTER_SET_PASSCODE = 'REGISTER_SET_PASSCODE';
export const REGISTER_SET_STORE_LOGO = 'REGISTER_SET_STORE_LOGO';
export const REGISTER_SET_STORE_NAME = 'REGISTER_SET_STORE_NAME';
export const REGISTER_SET_STORE_TAGLINE = 'REGISTER_SET_STORE_TAGLINE';
export const REGISTER_SET_STORE_PHONE = 'REGISTER_SET_STORE_PHONE';
export const REGISTER_SET_STORE_CATEGORYID = 'REGISTER_SET_STORE_CATEGORYID';
export const REGISTER_SET_STORE_LATITUDE = 'REGISTER_SET_STORE_LATITUDE';
export const REGISTER_SET_STORE_LONGITUDE = 'REGISTER_SET_STORE_LONGITUDE';
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

export const resetRegisterAction = () => (dispatch: Function) => {
  dispatch({type: REGISTER_RESET});
};

export const setEmailAction = (email: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_EMAIL,
      email,
    });
  };
};

export const setPasswordAction = (password: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_PASSWORD,
      password,
    });
  };
};

export const setPasscodeAction = (passcode: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_PASSCODE,
      passcode,
    });
  };
};

export const setStoreLogoAction = (storeLogo: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_LOGO,
      storeLogo,
    });
  };
};

export const setStoreNameAction = (storeName: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_NAME,
      storeName,
    });
  };
};

export const setStoreTaglineAction = (storeTagline: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_TAGLINE,
      storeTagline,
    });
  };
};

export const setStorePhoneAction = (storePhone: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_PHONE,
      storePhone,
    });
  };
};

export const setStoreCategoryIDAction = (categoryID: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_CATEGORYID,
      categoryID,
    });
  };
};

export const setStoreLatitudeAction = (storeLatitude: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_LATITUDE,
      storeLatitude,
    });
  };
};

export const setStoreLongitudeAction = (storeLongitude: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_STORE_LONGITUDE,
      storeLongitude,
    });
  };
};

export const registerAccountAndStoreAction = () => async (
  dispatch: Function,
  getState: any,
) => {
  const {register} = getState();
  const {
    email, 
    password,
    storeLogo,
    storeName,
    storeTagline,
    storePhone,
    storeLatitude,
    storeLongitude,
    categoryID,
  } = register;

  dispatch({type: REGISTER_REQUEST});

  const response = await auth().createUserWithEmailAndPassword(email, password);

  if (response.user) {

    const accessToken = (await response.user?.getIdToken()).toString();
    const messagingToken = (await messaging().getToken()).toString();

    const storeInput: any = { 
      logo: storeLogo,
      name: storeName,
      tagline: storeTagline,
      phone: storePhone,
      latitude: storeLatitude,
      longitude: storeLongitude,
      categoryID: categoryID,
    }

    await client.mutate({
      variables: { input: storeInput },
      mutation: REQUEST_CREATE_STORE,
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });

    setTokenAction(accessToken)(dispatch);
    setMessagingTokenAction(accessToken, messagingToken)(dispatch);
    
    return dispatch({type: REGISTER_SUCCESS});
  }

  return dispatch({type: REGISTER_ERROR, error: {message: 'Something wrong!'}});
};
