export interface IStore {
  id: string;
  logo: string;
  name: string;
  tagline: string;
  phone: string;
  latitude: number;
  longitude: number;
  category: object;
}
