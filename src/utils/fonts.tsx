import colors from './colors';

export default {
  h1Black: {
    fontFamily: 'DBHeavent',
    fontSize: 40,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  h1Green: {
    fontFamily: 'DBHeavent',
    fontSize: 40,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.primaryColor
  },
  h1BoldWhite: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 40,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fourthColor
  },
  h1BoldBlack: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 40,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  h2White: {
    fontFamily: 'DBHeavent',
    fontSize: 30,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fourthColor
  },
  h2Green: {
    fontFamily: 'DBHeavent',
    fontSize: 30,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.primaryColor
  },
  h2Black: {
    fontFamily: 'DBHeavent',
    fontSize: 30,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  h2Bold: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 30,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  h3White: {
    fontFamily: 'DBHeavent',
    fontSize: 27,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fourthColor
  },
  h3Green: {
    fontFamily: 'DBHeavent',
    fontSize: 27,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.primaryColor
  },
  h3: {
    fontFamily: 'DBHeavent',
    fontSize: 27,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  h3Bold: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 27,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  placeholder: {
    fontFamily: 'DBHeavent',
    fontSize: 25,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.iconColor
  },
  paragraphWhite: {
    fontFamily: 'DBHeavent',
    fontSize: 25,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fourthColor
  },
  paragraphGreen: {
    fontFamily: 'DBHeavent',
    fontSize: 25,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.primaryColor
  },
  paragraph: {
    fontFamily: 'DBHeavent',
    fontSize: 25,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  paragraphBold: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 25,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  subparagraphWhite: {
    fontFamily: 'DBHeavent',
    fontSize: 25,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fourthColor
  },
  subparagraph: {
    fontFamily: 'DBHeavent',
    fontSize: 20,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
  subparagraphBold: {
    fontFamily: 'DBHeavent-Med',
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    color: colors.fontColor
  },
};