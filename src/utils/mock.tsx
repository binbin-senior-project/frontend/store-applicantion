import logo from '@images/PromotionShopIcon/dunkin-donut-icon.png';

export const coupons = [
  {
    id: 1,
    logo,
    photo: 'https://i.imgur.com/U8bqoaG.jpg',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    storeName: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    status: 'expire',
  },
  {
    id: 2,
    logo,
    photo: 'https://i.imgur.com/McpsT3P.png',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    storeName: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    status: 'available',
  },
  {
    id: 3,
    logo,
    photo: 'https://i.imgur.com/cJYjvKP.png',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    storeName: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    status: 'pending',
  },
];
