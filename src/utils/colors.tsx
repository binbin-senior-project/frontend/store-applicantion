export default {
  primaryColor: '#005945',
  secondaryColor: '#007459',
  thirdaryColor: '#ffc410',
  fourthColor: '#ffffff',
  fontColor: '#222222',
  iconColor: '#d1d1d1',
  inputBackgroundColor: '#eaeaea',
  pageBackgroundColor: '#f0f0f0',
  whiteBorder: '#f2f2f2',
  grayBorder: '#f1f1f1',
  redColor: '#ff003e',
  whiteColor: '#ffffff',
};
