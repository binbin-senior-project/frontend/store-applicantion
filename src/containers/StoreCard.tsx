import React from 'react';
import {Image} from 'react-native';
import {connect} from 'react-redux';
import {Col, H1} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  store: any;
  navigation: any;
}

const StoreCard = ({store, navigation}: Props) => {
  return (
    <Col
      center
      style={{
        backgroundColor: colors.whiteColor,
        paddingVertical: 50,
        marginVertical: 10,
      }}>
      <Image
        source={{uri: store.logo}}
        style={{width: 100, height: 100, borderRadius: 100}}
      />
      <H1 center>{store.name}</H1>
    </Col>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
});

export default connect(mapStateToProps)(StoreCard);
