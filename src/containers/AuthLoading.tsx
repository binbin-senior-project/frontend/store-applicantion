import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import auth from '@react-native-firebase/auth';
import {setTokenAction, setMessagingTokenAction} from '@actions/auth';
import {loadCurrentStoreProfileAction} from '@actions/store';
import LoaderDefault from '@components/loaders/LoaderDefault';

interface Props {
  setToken: Function;
  setMessagingToken: Function;
  loadCurrentStoreProfile: Function;
  navigation: any;
}

const AuthLoading = ({
  setToken,
  setMessagingToken,
  loadCurrentStoreProfile,
  navigation,
}: Props) => {
  const [_isLoading, _setLoading] = useState(true);

  const loadToken = async () => {
    try {
      const user = await auth().currentUser;

      messaging().requestPermission();

      if (user) {
        const accessToken = (await user.getIdToken()).toString();
        const messagingToken = (await messaging().getToken()).toString();

        setToken(accessToken);
        setMessagingToken(accessToken, messagingToken);

        setTimeout(() => {
          loadCurrentStoreProfile();

          setTimeout(() => {
            _setLoading(false);
            navigation.navigate('Main');
          }, 500);
        }, 200);

        return;
      }
    } catch {
      _setLoading(false);
      return navigation.navigate('RegisterStack');
    }

    _setLoading(false);
    return navigation.navigate('RegisterStack');
  };

  useEffect(() => {
    loadToken();
  }, []);

  return <LoaderDefault isLoading={_isLoading} />;
};

const mapDispatchToProps = (dispatch: Function) => ({
  setToken: (accessToken: string) => dispatch(setTokenAction(accessToken)),
  setMessagingToken: (accessToken: string, token: string) =>
    dispatch(setMessagingTokenAction(accessToken, token)),
  loadCurrentStoreProfile: () => dispatch(loadCurrentStoreProfileAction()),
});

export default connect(
  null,
  mapDispatchToProps,
)(AuthLoading);
