import React from 'react';
import {StatusBar, TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Row, H1, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  title: string;
  route: any;
  navigation: any;
}

const AppbarCouponDetail = ({title, route, navigation}: Props) => {
  return (
    <Row
      between
      alignCenter
      style={{
        width: '100%',
        paddingHorizontal: 20,
        paddingTop: 35,
        backgroundColor: colors.primaryColor,
      }}>
      <StatusBar barStyle="light-content" />
      <Row alignCenter>
        <Icon
          name="arrow-left"
          size={25}
          color={colors.whiteColor}
          style={{marginRight: 20}}
          onPress={() => navigation.goBack()}
        />
        <H1 white style={{marginVertical: 10}}>
          {title}
        </H1>
      </Row>
      <Row>
        <TouchableWithoutFeedback
          onPress={() =>
            navigation.navigate('EditCouponScreen', {
              couponID: route.params.couponID,
            })
          }>
          <P white>แก้ไข</P>
        </TouchableWithoutFeedback>
      </Row>
    </Row>
  );
};

export default AppbarCouponDetail;
