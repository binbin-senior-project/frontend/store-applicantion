import React from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import {H1, Row} from '@components/Global';
import AvatarButton from '@components/buttons/AvatarButton';
import colors from '@utils/colors';

interface Props {
  title: string;
  store: string;
  navigation: any;
}

const AppbarDefault = ({title, store, navigation}: Props) => {
  return (
    <Row
      alignCenter
      style={{
        paddingHorizontal: 20,
        paddingTop: 45,
        paddingBottom: 10,
        backgroundColor: colors.primaryColor,
      }}>
      <View style={{marginRight: 20}}>
        <AvatarButton
          avatar={store.logo}
          onPress={() => navigation.navigate('ProfileScreen')}
        />
      </View>
      <H1 white>{title}</H1>
    </Row>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
});

export default connect(mapStateToProps)(AppbarDefault);
