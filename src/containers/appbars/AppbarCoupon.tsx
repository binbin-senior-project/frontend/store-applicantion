import React from 'react';
import {StatusBar, TouchableWithoutFeedback} from 'react-native';
import {connect} from 'react-redux';
import AvatarButton from '@components/buttons/AvatarButton';
import {Row, H1, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  title: string;
  store: string;
  navigation: any;
}

const AppbarCoupon = ({title, store, navigation}: Props) => {
  return (
    <Row
      between
      alignCenter
      style={{
        width: '100%',
        paddingHorizontal: 20,
        paddingTop: 35,
        backgroundColor: colors.primaryColor,
      }}>
      <StatusBar barStyle="light-content" />
      <Row alignCenter>
        <AvatarButton
          avatar={store.logo}
          onPress={() => navigation.navigate('ProfileScreen')}
        />
        <H1 white style={{margin: 10}}>
          {title}
        </H1>
      </Row>
      <Row>
        <TouchableWithoutFeedback
          onPress={() => navigation.navigate('CreateCouponStack')}>
          <P white>สร้างคูปอง</P>
        </TouchableWithoutFeedback>
      </Row>
    </Row>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
});

export default connect(mapStateToProps)(AppbarCoupon);
