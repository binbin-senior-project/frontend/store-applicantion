import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  KeyboardAvoidingView,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import MapView from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {
  Container,
  Col,
  Footer,
  H1,
  H3,
  Content,
  Row,
  Center,
} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Button from '@components/buttons/Button';
import LoaderDefault from '@components/loaders/LoaderDefault';
import MyMarker from '@components/Marker';
import {
  setStoreLatitudeAction,
  setStoreLongitudeAction,
  registerAccountAndStoreAction,
} from '@actions/register';
import colors from '@utils/colors';
import {loadCurrentStoreProfileAction} from '@actions/store';

interface Props {
  storeLogo: string;
  isRegistered: boolean;
  setStoreLatitude: Function;
  setStoreLongitude: Function;
  registerAccountAndStore: Function;
  loadCurrentStoreProfile: Function;
  navigation: any;
}

const RegisterStep3 = ({
  storeLogo,
  isRegistered,
  setStoreLatitude,
  setStoreLongitude,
  registerAccountAndStore,
  loadCurrentStoreProfile,
  navigation,
}: Props) => {
  const [_latitude, _setLatitude] = useState(0);
  const [_longitude, _setLongitude] = useState(0);
  const [_isLoading, _setLoading] = useState(false);
  const [_isMapLoading, _setMapLoading] = useState(false);

  const onRegionChange = (region: any) => {
    _setLatitude(region.latitude);
    _setLongitude(region.longitude);
  };

  const onNext = async () => {
    _setLoading(true);

    setStoreLatitude(_latitude);
    setStoreLongitude(_longitude);
    await registerAccountAndStore();

    _setLoading(false);
  };

  useEffect(() => {
    _setMapLoading(true);

    const unsubscribe = navigation.addListener('focus', () => {
      Geolocation.getCurrentPosition(pos => {
        _setLatitude(pos.coords.latitude);
        _setLongitude(pos.coords.longitude);

        _setMapLoading(false);
      });
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (isRegistered) {
      loadCurrentStoreProfile();

      setTimeout(() => {
        _setLoading(false);
        navigation.navigate('Main');
      }, 500);
    }
  }, [isRegistered]);

  return (
    <SafeAreaView>
      <KeyboardAvoidingView behavior="padding">
        <StatusBar barStyle="dark-content" />
        <LoaderDefault isLoading={_isLoading} />
        <Container disablePadding>
          <Col style={{paddingTop: 20, paddingHorizontal: 20}}>
            <Icon
              name="arrow-left"
              size={35}
              onPress={() => navigation.pop()}
              style={{marginBottom: 10}}
            />
          </Col>
          <Content>
            <Col style={{paddingHorizontal: 20}}>
              <H1>ที่ตั้งกิจการ</H1>
              <H3 style={{marginBottom: 20}}>
                ลูกค้าจะรู้จักกิจการของคุณมากขึ้น
              </H3>
            </Col>
            {_isMapLoading ? (
              <Center>
                <ActivityIndicator color={colors.primaryColor} />
              </Center>
            ) : (
              <View
                style={{
                  position: 'relative',
                  borderTopWidth: 1,
                  borderTopColor: colors.inputBackgroundColor,
                  height: '100%',
                }}>
                <MapView
                  initialRegion={{
                    latitude: _latitude,
                    longitude: _longitude,
                    latitudeDelta: 0.0009,
                    longitudeDelta: 0.009,
                  }}
                  style={{
                    width: '100%',
                    height: '105%',
                  }}
                  onRegionChange={onRegionChange}
                />
                <Row style={{position: 'absolute', top: '35%', left: '45%'}}>
                  <MyMarker source={{uri: storeLogo}} />
                </Row>
              </View>
            )}
          </Content>
          <Footer style={{paddingHorizontal: 20}}>
            <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
          </Footer>
        </Container>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state: any) => ({
  isRegistered: state.register.isRegistered,
  storeLogo: state.register.storeLogo,
});

const mapDispatchToProps = (dispatch: Function) => ({
  setStoreLatitude: (value: string) => dispatch(setStoreLatitudeAction(value)),
  setStoreLongitude: (value: string) =>
    dispatch(setStoreLongitudeAction(value)),
  registerAccountAndStore: () => dispatch(registerAccountAndStoreAction()),
  loadCurrentStoreProfile: () => dispatch(loadCurrentStoreProfileAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterStep3);
