import React, {useState} from 'react';
import {KeyboardAvoidingView, StatusBar} from 'react-native';
import {connect} from 'react-redux';
import validator from 'validator';
import {Container, Footer, H1, H3, Content} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import Error from '@components/Error';
import {setEmailAction, setPasswordAction} from '@actions/register';

interface RegisterStep3Props {
  setEmail: Function;
  setPassword: Function;
  navigation: any;
}

const RegisterStep1 = ({
  setEmail,
  setPassword,
  navigation,
}: RegisterStep3Props) => {
  const [_email, _setEmail] = useState('');
  const [_password, _setPassword] = useState('');
  const [_confirmPassword, _setConfirmpassword] = useState('');
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');

  const onNext = () => {
    if (!validator.isEmail(_email)) {
      _setError(true);
      _setErrorMsg('อีเมล์ไม่ถูกต้อง');
      return;
    }

    if (!_password.match(/(?=.*?[A-Za-z0-9])(?=.*?[^\w\s]).{7,}/)) {
      _setError(true);
      _setErrorMsg(
        '- รหัสผ่านควรยาว 7 ตัวอักษร\n- มีตัวอักษรใหญ่อย่างน้อย 1 ตัว\n- อักษรพิเศษอย่างน้อย 1 ตัว',
      );
      return;
    }

    if (
      validator.isEmpty(_password) ||
      validator.isEmpty(_confirmPassword) ||
      !validator.equals(_password, _confirmPassword)
    ) {
      _setError(true);
      _setErrorMsg('รหัสผ่านไม่ตรงกัน');
      return;
    }

    setEmail(_email);
    setPassword(_password);
    navigation.navigate('RegisterStep2');
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Container style={{paddingTop: 60}}>
        <Icon
          name="arrow-left"
          size={35}
          onPress={() => navigation.pop()}
          style={{marginBottom: 10}}
        />
        <Content>
          <H1>สร้างบัญชี</H1>
          <H3 style={{marginBottom: 20}}>
            ระบุอีเมล์และรหัสผ่านเพื่อเข้าใช้งาน
          </H3>
          <Input
            type="email"
            placeholder="อีเมล"
            value={_email}
            onChangeText={(text: string) => _setEmail(text)}
          />
          <Input
            type="password"
            placeholder="รหัสผ่าน"
            value={_password}
            onChangeText={(text: string) => _setPassword(text)}
          />
          <Input
            type="password"
            placeholder="ยืนยันรหัสผ่าน"
            value={_confirmPassword}
            onChangeText={(text: string) => _setConfirmpassword(text)}
          />
        </Content>
        <Footer>
          <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setEmail: (value: string) => dispatch(setEmailAction(value)),
  setPassword: (value: string) => dispatch(setPasswordAction(value)),
});

export default connect(
  null,
  mapDispatchToProps,
)(RegisterStep1);
