import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import {connect} from 'react-redux';
import validator from 'validator';
import Error from '@components/Error';
import {Container, Row, Center, H3} from '@components/Global';
import Logo from '@components/logos/Logo';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import LoaderDefault from '@components/loaders/LoaderDefault';
import {loginAction} from '@actions/auth';
import {loadCurrentStoreProfileAction} from '@actions/store';

interface LoginProps {
  isLoggedIn: boolean;
  login: Function;
  loadCurrentStoreProfile: Function;
  navigation: any;
}

const Login = ({
  isLoggedIn,
  login,
  loadCurrentStoreProfile,
  navigation,
}: LoginProps) => {
  const [_email, _setEmail] = useState('');
  const [_password, _setPassword] = useState('');
  const [_isLoading, _setLoading] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');

  const onLogin = async () => {
    if (_email == '' || !validator.isEmail(_email)) {
      _setError(true);
      _setErrorMsg('อีเมล์ไม่ถูกต้อง');
      return;
    }

    if (
      _password == '' ||
      !_password.match(/(?=.*?[A-Za-z0-9])(?=.*?[^\w\s]).{7,}/)
    ) {
      _setError(true);
      _setErrorMsg(
        '- รหัสผ่านควรยาว 7 ตัวอักษร\n- มีตัวอักษรใหญ่อย่างน้อย 1 ตัว\n- อักษรพิเศษอย่างน้อย 1 ตัว',
      );
      return;
    }

    _setLoading(true);

    try {
      await login(_email, _password);
    } catch {
      _setLoading(false);
      _setError(true);
      _setErrorMsg('อีเมลหรือรหัสผ่านไม่ถูกต้อง');
      return;
    }
  };

  const goToRegister = () => {
    navigation.navigate('RegisterStep1');
  };

  useEffect(() => {
    if (isLoggedIn) {
      loadCurrentStoreProfile();

      setTimeout(() => {
        _setLoading(false);
        navigation.navigate('Main');
      }, 500);
    }
  }, [isLoggedIn]);

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Container>
        <Center>
          <Logo />
        </Center>
        <View style={{height: 300}}>
          <Input
            placeholder="อีเมล"
            value={_email}
            onChangeText={(text: string) => _setEmail(text)}
          />
          <Input
            type="password"
            placeholder="รหัสผ่าน"
            value={_password}
            onChangeText={(text: string) => _setPassword(text)}
          />
          <Button text="เข้าสู่ระบบ" onPress={onLogin} />
          <Row center>
            <H3>หากยังไม่ได้เป็นสมาชิก</H3>
            <TouchableOpacity onPress={goToRegister} style={{marginLeft: 5}}>
              <H3 green>สมัครสมาชิก</H3>
            </TouchableOpacity>
          </Row>
        </View>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state: any) => ({
  isLoggedIn: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch: Function) => ({
  login: (email: string, password: string) =>
    dispatch(loginAction(email, password)),
  loadCurrentStoreProfile: () => dispatch(loadCurrentStoreProfileAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
