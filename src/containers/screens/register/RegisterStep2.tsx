import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  StatusBar,
  Modal,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import validator from 'validator';
import {useQuery} from '@apollo/react-hooks';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';
import {Row, Container, Footer, H1, H3, Content} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import Panel from '@components/panels/Panel';
import Radio from '@components/forms/Radio';
import Select from '@components/forms/Select';
import UploadButton from '@components/buttons/UploadButton';
import Error from '@components/Error';
import LoaderDefault from '@components/loaders/LoaderDefault';
import {GET_STORE_CATEGORIES} from '@graphql/query';
import {
  setStoreLogoAction,
  setStoreNameAction,
  setStoreTaglineAction,
  setStorePhoneAction,
  setStoreCategoryIDAction,
} from '@actions/register';

interface Props {
  setStoreLogo: Function;
  setStoreName: Function;
  setStoreTagline: Function;
  setStorePhone: Function;
  setStoreCategoryID: Function;
  navigation: any;
}

const RegisterStep2 = ({
  setStoreLogo,
  setStoreName,
  setStoreTagline,
  setStorePhone,
  setStoreCategoryID,
  navigation,
}: Props) => {
  const [_storeCategories, _setStoreCategories] = useState([]);
  const [_storeLogo, _setStoreLogo] = useState('');
  const [_storeName, _setStoreName] = useState('');
  const [_storePhone, _setStorePhone] = useState('');
  const [_storeTagline, _setStoreTagline] = useState('');
  const [_storeCategory, _setStoreCategory] = useState({});
  const [_isVisible, _setVisible] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');
  const [_isLoading, _setLoading] = useState(false);


  // Get Types
  useQuery(GET_STORE_CATEGORIES, {
    variables: {offset: 0, limit: 100},
    onCompleted: (data: any) => {
      if (data.getStoreCategories == null) return;

      const newStoreCategory = data.getStoreCategories.map((item: any) => {
        return {
          id: item.id,
          name: item.name,
        };
      });

      _setStoreCategories([..._storeCategories, ...newStoreCategory]);
    },
  });

  const onSelectCategory = (category: any) => {
    _setStoreCategory(category);
    _setVisible(false);
  };

  const onSelectImage = () => {
    const options = {
      title: 'เลือกรูปภาพ',
      maxWidth: 300,
      maxHeight: 300,
    }

    ImagePicker.showImagePicker(options, (response: any) => {
      if (response.didCancel || response.error) {
        return;
      }

      _setStoreLogo(response.uri);
    });
  };

  const onNext = async () => {

    if (validator.isEmpty(_storeLogo)) {
      _setError(true);
      _setErrorMsg('โปรดอัปโหลดโลโก้ร้าน');
      return;
    }

    if (validator.isEmpty(_storeName)) {
      _setError(true);
      _setErrorMsg('ระบุชื่อร้านให้ถูกต้อง');
      return;
    }

    if (validator.isEmpty(_storeTagline)) {
      _setError(true);
      _setErrorMsg('ระบุแท็กไลน์ให้ถูกต้อง');
      return;
    }


    if (!validator.isMobilePhone(_storePhone, 'th-TH')) {
      _setError(true);
      _setErrorMsg('ระบุมือถือให้ถูกต้อง');
      return;
    }

    if (!_storeCategory) {
      _setError(true);
      _setErrorMsg('ระบุหมวดหมู่ร้าน');
      return;
    }

    _setLoading(true);

    const photoName = uuid.v4();
    const reference = storage().ref(photoName);
    await reference.putFile(_storeLogo);
    const url = await storage().ref(photoName).getDownloadURL();

    setStoreLogo(url);
    setStoreName(_storeName);
    setStoreTagline(_storeTagline);
    setStorePhone(_storePhone);
    setStoreCategoryID(_storeCategory.id);

    _setLoading(false);

    navigation.navigate('RegisterStep3');
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Modal visible={_isVisible} animationType="slide">
        <SafeAreaView>
          <Panel title="เลือกประเภทร้านค้า">
            {_storeCategories.map(item => (
              <Radio title={item.name} onPress={() => onSelectCategory(item)} />
            ))}
          </Panel>
        </SafeAreaView>
      </Modal>
      <Container style={{paddingTop: 60}}>
        <Icon
          name="arrow-left"
          size={35}
          onPress={() => navigation.pop()}
          style={{marginBottom: 10}}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Content>
            <H1>สร้างร้านค้า</H1>
            <H3 style={{marginBottom: 20}}>
              ลูกค้าจะรู้จักกิจการของคุณมากขึ้น
            </H3>
            <Row center>
              <UploadButton avatar={_storeLogo} onPress={onSelectImage} />
            </Row>
            <Input
              placeholder="ชื่อร้าน"
              value={_storeName}
              onChangeText={(text: string) => _setStoreName(text)}
            />
            <Input
              placeholder="คำฮิตประจำร้าน (แท็กไลน์)"
              value={_storeTagline}
              onChangeText={(text: string) => _setStoreTagline(text)}
            />
            <Input
              type="number"
              placeholder="เบอร์ติดต่อร้าน"
              value={_storePhone}
              onChangeText={(text: string) => _setStorePhone(text)}
            />
            <Select
              placeholder="ประเภทร้าน"
              onPress={() => _setVisible(true)}
              value={_storeCategory?.name}
            />
          </Content>
        </ScrollView>
        <Footer>
          <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setStoreLogo: (value: string) => dispatch(setStoreLogoAction(value)),
  setStoreName: (value: string) => dispatch(setStoreNameAction(value)),
  setStoreTagline: (value: string) => dispatch(setStoreTaglineAction(value)),
  setStorePhone: (value: string) => dispatch(setStorePhoneAction(value)),
  setStoreCategoryID: (value: string) =>
    dispatch(setStoreCategoryIDAction(value)),
});

export default connect(
  null,
  mapDispatchToProps,
)(RegisterStep2);
