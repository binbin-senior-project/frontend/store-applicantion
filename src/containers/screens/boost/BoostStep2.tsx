import React, {useState, useEffect} from 'react';
import {ScrollView} from 'react-native';
import {connect} from 'react-redux';
import validator from 'validator';
import {Row} from '@components/Global';
import Button from '@components/buttons/Button';
import Panel from '@components/panels/Panel';
import Error from '@components/Error';
import {useLazyQuery, useMutation} from '@apollo/react-hooks';
import {GET_COUPON_PRODUCTS} from '@graphql/query';
import ProductList from '@components/ProductList';
import{REQUEST_COUPON_BOOST} from '@graphql/mutation';
import LoaderDefault from '@components/loaders/LoaderDefault';

interface Props {
  route: any;
  accessToken: String;
  navigation: any;
}

const BoostStep2 = ({route, accessToken, navigation}: Props) => {
  const [_products, _setProducts] = useState([]);
  const [_product, _setProduct] = useState('');
  const [_isLoading, _setLoading] = useState(false);
  const [_isProductLoading, _setProductLoading] = useState(true);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');
  const [_requestBoostCoupon] = useMutation(REQUEST_COUPON_BOOST, {
    onCompleted: () => {
      _setLoading(false);
      navigation.navigate('BoostStep3');
    },
  })

  const [getCouponProducts] = useLazyQuery(GET_COUPON_PRODUCTS, {
    onCompleted: (data: any) => {
      if (data?.getCouponProducts == null) return;

      const products = data?.getCouponProducts.map((item: any) => {
        item.checked = false;
        return item;
      });

      _setProducts(products);
      _setProductLoading(false);
    },
  });


  const onItemPress = (productCoupon: any) => {
    const products: any = _products.map((item: any) => {
      item.checked = false;

      if (item.id == productCoupon.id) {
        productCoupon.checked = true;
      }

      return item;
    })

    _setProduct(productCoupon.id);
    _setProducts(products);
  };

  const onComplete = () => {
    _setLoading(true);

    if (validator.isEmpty(_product)) {
      _setLoading(false);
      _setError(true);
      _setErrorMsg('โปรดเลือกแพ็กเกจ');
      return;
    }
    _requestBoostCoupon({
      variables: {
        input: {
          couponIDs: route.params.boostCoupons,
          productID: _product,
        },
      },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getCouponProducts({
        variables: { offset: 0, limit: 100 },
        context: {headers: {authorization: `Bearer ${accessToken}`}},
      });
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <>
      <ScrollView showsVerticalScrollIndicator={false}>
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
        <Panel title="เลือกแพ็คเกจ">
          <ProductList
            products={_products}
            scrollEnabled={false}
            onItemPress={onItemPress}
            onEndReached={() => {}}
            isLoading={_isProductLoading}
          />
        </Panel>
      </ScrollView>
      <Row style={{padding: 20}}>
        <Button
          text="ส่งคำขอ"
          onPress={() => onComplete()}
        />
      </Row>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(BoostStep2);
