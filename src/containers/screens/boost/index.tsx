import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import AppbarBack from '@components/appbars/AppbarBack';
import BoostStep1 from './BoostStep1';
import BoostStep2 from './BoostStep2';
import BoostStep3 from './BoostStep3';

const Stack = createStackNavigator();

const BoostStack = () => {
  return (
    <Stack.Navigator initialRouteName="BoostStep1" headerMode="screen">
      <Stack.Screen
        name="BoostStep1"
        component={BoostStep1}
        options={{
          header: props => <AppbarDefault title="บูสต์คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="BoostStep2"
        component={BoostStep2}
        options={{
          header: props => <AppbarBack title="เลือกแพ็กเกจ" {...props} />,
        }}
      />
      <Stack.Screen
        name="BoostStep3"
        component={BoostStep3}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default BoostStack;
