import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import CouponBoostList from '@components/CouponBoostList';
import {Row, Container} from '@components/Global';
import Button from '@components/buttons/Button';
import Error from '@components/Error';
import {GET_APPROVE_COUPONS} from '@graphql/query';
import {useLazyQuery} from '@apollo/react-hooks';
import {IStore} from '@interfaces/store';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  store: IStore;
  navigation: any;
}

const BoostStep1 = ({
  accessToken,
  store,
  navigation,
}: Props) => {
  const [_coupons, _setCoupons] = useState([]);
  const [_isLoading, _setLoading] = useState(false);
  const [_offset, _setOffset] = useState(0);
  const [_boostCoupons, _setBoostCoupons] = useState([]);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');


  const renderNewCoupon = (couponData: any) => {
    if (couponData == null) return;

    const coupons = couponData.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        status: item.status,
        checked: false,
      };
    });

    return coupons;
  };

  const [_getApproveCoupons] = useLazyQuery(GET_APPROVE_COUPONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getApproveCoupons == null) return;

      const coupons = renderNewCoupon(data.getApproveCoupons);
      _setCoupons(coupons);
      _setOffset(0);
    },
  });

  const [_loadMoreApproveCoupons] = useLazyQuery(GET_APPROVE_COUPONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getApproveCoupons == null) return;

      const coupons = renderNewCoupon(data.getApproveCoupons);
      _setCoupons([..._coupons, ...coupons]);
    },
  });

  const onItemPress = (couponID: string) => {
    let customCoupons = _coupons.map(coupon => {
      if (coupon.id == couponID && coupon.checked) {
        coupon.checked = false;

        const temp = [..._boostCoupons];
        const index = temp.indexOf(coupon.id);
        temp.splice(index, 1);

        _setBoostCoupons(temp);
      } else if (coupon.id == couponID && !coupon.checked) {
        coupon.checked = true;

        _setBoostCoupons([..._boostCoupons, coupon.id]);
      }

      return coupon;
    });

    _setCoupons(customCoupons);
  };

  const onNext = () => {
    if (_boostCoupons.length == 0) {
      _setError(true);
      _setErrorMsg('โปรดเลือกคูปอง');
      return;
    }
    
    navigation.navigate('BoostStep2', { boostCoupons: _boostCoupons });
  };

  const onRefresh = () => {
    _getApproveCoupons({
      variables: {storeID: store.id, offset: 0, limit: 6},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  }

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      _getApproveCoupons({
        variables: {storeID: store.id, offset: 0, limit: 6},
        context: {headers: {authorization: `Bearer ${accessToken}`}},
      });
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (_offset == 0) return;
    
    _loadMoreApproveCoupons({
      variables: {storeID: store.id, offset: _offset, limit: 6},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  }, [_offset])

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      _setBoostCoupons([]);
      _setOffset(0);
    });

    return unsubscribe;
  }, [navigation])
  
  return (
    <Container disablePadding>
       <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <CouponBoostList
        isLoading={_isLoading}
        coupons={_coupons}
        onRefresh={onRefresh}
        onEndReached={onEndReached}
        onItemPress={onItemPress}
        scrollEnabled
      />
      {_coupons.length > 0 && (
        <Row
          style={{
            paddingVertical: 5,
            paddingHorizontal: 20,
            backgroundColor: colors.whiteColor,
            borderTopWidth: 1,
            borderColor: colors.inputBackgroundColor,
          }}
        >
          <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
        </Row>
      )}
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
  accessToken: state.auth.accessToken,
});

export default connect(
  mapStateToProps,
)(BoostStep1);
