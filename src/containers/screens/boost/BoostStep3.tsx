import React, { useState, useEffect } from 'react';
import {StatusBar, ActivityIndicator} from 'react-native';
import { connect } from 'react-redux';
import { StackActions } from '@react-navigation/native';
import moment from 'moment';
import {
  Container,
  Content,
  Row,
  Col,
  Footer,
  H1,
  H3,
  P,
} from '@components/Global';
import Logo from '@components/logos/Logo';
import Divider from '@components/Divider';
import Button from '@components/buttons/Button';
import { useLazyQuery } from '@apollo/react-hooks';
import { GET_TRANSACTIONS } from '@graphql/query';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  store: any;
  navigation: any;
}

const BoostStep3 = ({ accessToken, store, navigation}: Props) => {
  const [_transaction, _setTransaction] = useState(null);
  const [_isLoading, _setLoading] = useState(true);
  const [_getTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      const { id, point, description, createdAt } = data?.getTransactions[0];

      const d = Date.parse(createdAt.replace(/-/gi, '/').split('.')[0]);
      const dd = moment(d).locale('th');
  
      _setTransaction({
        id,
        point,
        description,
        date: dd.add(543, 'y').format('ll'),
        time: dd.format('HH:mm')
      })
    }
  });

  useEffect(() => {
    _getTransactions({
      variables: { storeID: store.id, offset: 0, limit: 1 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })
  }, [navigation])

  if (_isLoading) {
    return (
      <Container>
        <ActivityIndicator color={colors.primaryColor} />
      </Container>
    );
  }

  return (
    <Container>
      <StatusBar barStyle="dark-content" />
      <Content style={{justifyContent: 'center'}}>
        <Col center>
          <Logo width={100} />
          <H1>สำเร็จ</H1>
          <H3>ส่งคำขอสร้างคูปองสำเร็จ</H3>
        </Col>
        <Divider />
        <Row between>
          <Content>
            <P bold>เลขรายการ</P>
            <P>{_transaction.id}</P>
          </Content>
        </Row>
        <Divider />
        <Row between>
          <Content>
            <P bold>วันที่</P>
            <P>{_transaction.date}</P>
          </Content>
          <Content>
            <P bold right>
              เวลา
            </P>
            <P right>{_transaction.time}</P>
          </Content>
        </Row>
        <Divider />
        <Row between>
          <Content>
            <P bold>รายละเอียด</P>
            <P>{_transaction.description}</P>
          </Content>
        </Row>
        <Divider />
      </Content>
      <Footer>
        <Button text="เรียบร้อย" onPress={() => navigation.dispatch(StackActions.popToTop())} />
      </Footer>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  store: state.store,
});

export default connect(mapStateToProps)(BoostStep3);

