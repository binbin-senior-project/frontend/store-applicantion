import React, {useState, useEffect, useLayoutEffect} from 'react';
import {connect} from 'react-redux';
import moment from 'moment';
import CouponList from '@components/CouponList';
import {Container} from '@components/Global';
import {GET_COUPONS} from '@graphql/query';
import {useLazyQuery} from '@apollo/react-hooks';
import {IStore} from '@interfaces/store';

interface Props {
  accessToken: String;
  store: IStore;
  navigation: any;
}

const HomeScreen = ({accessToken, store, navigation}: Props) => {
  const [_coupons, _setCoupons] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [_isLoading, _setLoading] = useState(false);

  const renderNewCoupon = (coupons: any) => {
    const newCoupons = coupons.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        status: item.status,
      };
    });

    return newCoupons;
  };

  const [_getCoupons] = useLazyQuery(GET_COUPONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);
      
      if (data?.getCoupons == null) return;

      const coupons = renderNewCoupon(data.getCoupons);
      _setCoupons(coupons);
      _setOffset(0);
    },
  });

  const [_loadMoreCoupons] = useLazyQuery(GET_COUPONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getCoupons == null) return;

      const coupons = renderNewCoupon(data.getCoupons);
      _setCoupons([..._coupons, ...coupons]);
    },
  });

  const onRefresh = () => {
    _getCoupons({
      variables: {storeID: store.id, offset: 0, limit: 6},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  }

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  useEffect(() => {
    _setLoading(true);

    const unsubscribe = navigation.addListener('focus', () => {
      _getCoupons({
        variables: {storeID: store.id, offset: 0, limit: 6},
        context: {headers: {authorization: `Bearer ${accessToken}`}},
      });
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (_offset == 0) return;

    _loadMoreCoupons({
      variables: {storeID: store.id, offset: _offset, limit: 6},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    })
  }, [_offset])


  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
     _setOffset(0);
    });

    return unsubscribe;
  }, [navigation])

  return (
    <Container disablePadding>
      <CouponList
        isLoading={_isLoading}
        coupons={_coupons}
        onRefresh={onRefresh}
        onEndReached={onEndReached}
      />
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(HomeScreen);
