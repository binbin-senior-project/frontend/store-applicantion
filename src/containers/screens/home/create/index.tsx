import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppbarBack from '@components/appbars/AppbarBack';
import CreateStep1 from './CreateStep1';
import CreateStep2 from './CreateStep2';

const Stack = createStackNavigator();

const CreateCouponStack = () => {
  return (
    <Stack.Navigator initialRouteName="CreateStep1" headerMode="screen">
      <Stack.Screen
        name="CreateStep1"
        component={CreateStep1}
        options={{
          header: props => <AppbarBack title="สร้างคูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="CreateStep2"
        component={CreateStep2}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default CreateCouponStack;
