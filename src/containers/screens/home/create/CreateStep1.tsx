import React, {useState, useEffect} from 'react';
import {SafeAreaView, ScrollView, Modal, StatusBar} from 'react-native';
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import uuid from 'react-native-uuid';
import storage from '@react-native-firebase/storage';
import validator from 'validator';
import {Container, Content, Footer} from '@components/Global';
import Input from '@components/forms/Input';
import Radio from '@components/forms/Radio';
import Select from '@components/forms/Select';
import Panel from '@components/panels/Panel';
import PanelTransparent from '@components/panels/PanelTransparent';
import UploadCouponButton from '@components/buttons/UploadCouponButton';
import Button from '@components/buttons/Button';
import LoaderDefault from '@components/loaders/LoaderDefault';
import Error from '@components/Error';
import {GET_COUPON_CATEGORIES} from '@graphql/query';
import {REQUEST_CREATE_COUPON} from '@graphql/mutation';
import {IStore} from '@interfaces/store';
import {useQuery, useMutation} from '@apollo/react-hooks';

interface Props {
  accessToken: string;
  store: IStore;
  navigation: any;
}

const CreateStep1 = ({accessToken, store, navigation}: Props) => {
  const [_photo, _setPhoto] = useState('');
  const [_name, _setName] = useState('');
  const [_description, _setDescription] = useState('');
  const [_point, _setPoint] = useState(0);
  const [_condition, _setCondition] = useState('');
  const [_couponCategory, _setCouponCategory] = useState({});
  const [_couponCategories, _setCouponCategories] = useState([]);
  const [_isVisible, _setVisible] = useState(false);
  const [_isLoading, _setLoading] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');


  const [_createCoupon] = useMutation(REQUEST_CREATE_COUPON, {
    onCompleted: () => {
      _setLoading(false);
      navigation.push('CreateStep2');
    },
    onError: () => {
      _setLoading(false);
    },
  });

  // Get Categories
  useQuery(GET_COUPON_CATEGORIES, {
    variables: {offset: 0, limit: 100},
    context: {headers: {authorization: `Bearer ${accessToken}`}},
    onCompleted: (data: any) => {
      if (data.getCouponCategories == null) return;

      const newCouponCategories = data.getCouponCategories.map((item: any) => {
        return {
          id: item.id,
          name: item.name,
        };
      });

      _setCouponCategories([..._couponCategories, ...newCouponCategories]);
    },
  });

  const onSelectCategory = (category: Object) => {
    _setCouponCategory(category);
    _setVisible(false);
  };

  const onSelectImage = () => {
    const options = {
      title: 'เลือกรูปภาพ',
      maxWidth: 300,
      maxHeight: 300,
    }

    ImagePicker.showImagePicker(options, (response: any) => {
      if (response.didCancel || response.error) {
        return;
      }

      _setPhoto(response.uri);
    });
  };

  const onComplete = async () => {

    if (validator.isEmpty(_photo)) {
      _setError(true);
      _setErrorMsg('โปรดอัปโหลดรูปโปรโมท');
      return;
    }

    if (validator.isEmpty(_name)) {
      _setError(true);
      _setErrorMsg('ระบุชื่อคูปอง');
      return;
    }

    if (validator.isEmpty(_description)) {
      _setError(true);
      _setErrorMsg('ระบุรายละเอียดคูปอง');
      return;
    }

    if (validator.isEmpty(_condition)) {
      _setError(true);
      _setErrorMsg('ระบุเงื่อนไข');
      return;
    }

    if (_point < 10) {
      _setError(true);
      _setErrorMsg('ระบุแต้มอย่างน้อย 10 แต้ม');
      return;
    }

    if (!_couponCategory) {
      _setError(true);
      _setErrorMsg('ระบุประเภทคูปอง');
      return;
    }

    _setLoading(true);

    const photoName = uuid.v4();
    const reference = storage().ref(photoName);
    await reference.putFile(_photo);
    const url = await storage().ref(photoName).getDownloadURL();

    _createCoupon({
      variables: {
        input: {
          name: _name,
          photo: url,
          description: _description,
          point: _point,
          condition: _condition,
          categoryID: _couponCategory.id,
          storeID: store.id,
        },
      },
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Modal visible={_isVisible} animationType="slide">
        <SafeAreaView>
          <StatusBar barStyle="dark-content" />
          <Panel title="เลือกประเภทร้านค้า">
            {_couponCategories?.map((item: any) => (
              <Radio
                key={item.id}
                title={item.name}
                onPress={() => onSelectCategory(item)}
              />
            ))}
          </Panel>
        </SafeAreaView>
      </Modal>
      <Container>
        <Content>
          <PanelTransparent title="รายละเอียด">
            <UploadCouponButton image={_photo} onPress={onSelectImage} />
            <Input
              placeholder="ชื่อคูปอง"
              value={_name}
              onChangeText={(text: string) => _setName(text)}
            />
            <Input
              placeholder="รายละเอียด"
              value={_description}
              onChangeText={(text: string) => _setDescription(text)}
            />
            <Input
              placeholder="เงื่อนไข"
              value={_condition}
              onChangeText={(text: string) => _setCondition(text)}
            />
            <Input
              placeholder="จำนวนแต้มแลก"
              type="number"
              value={_point}
              onChangeText={(text: string) => _setPoint(text)}
            />
            <Select
              placeholder="ประเภทคูปอง"
              value={_couponCategory.name}
              onPress={() => _setVisible(true)}
            />
          </PanelTransparent>
        </Content>
        <Footer>
          <Button text="ถัดไป" onPress={() => onComplete()} />
        </Footer>
      </Container>
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  store: state.store,
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(CreateStep1);
