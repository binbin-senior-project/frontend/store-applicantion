import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppbarCoupon from '@containers/appbars/AppbarCoupon';
import AppbarBack from '@components/appbars/AppbarBack';
import AppbarCouponDetail from '@containers/appbars/AppbarCouponDetail';
import HomeScreen from './HomeScreen';
import CouponDetailScreen from './CouponDetailScreen';
import CreateCouponStack from './create';
import EditCouponScreen from './EditCouponScreen';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="Home" headerMode="screen">
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{
          header: props => <AppbarCoupon title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="CouponDetailScreen"
        component={CouponDetailScreen}
        options={(props: any) => ({
          header: () => {
            return <AppbarCouponDetail title="คูปอง" {...props} />;
          },
        })}
      />
      <Stack.Screen
        name="CreateCouponStack"
        component={CreateCouponStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="EditCouponScreen"
        component={EditCouponScreen}
        options={props => ({
          header: () => <AppbarBack title="แก้ไขคูปอง" {...props} />,
        })}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
