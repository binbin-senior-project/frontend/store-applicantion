import React, { useState, useEffect } from 'react';
import {ActivityIndicator, View, Image, ScrollView} from 'react-native';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import {useLazyQuery} from '@apollo/react-hooks';
import {Container, Row, H1, P} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Panel from '@components/panels/Panel';
import colors from '@utils/colors';
import {GET_COUPON} from '@graphql/query';

interface Props {
  route: any;
  accessToken: string;
  navigation: any;
}

const CouponDetailScreen = ({route, accessToken, navigation}: Props) => {
  const { couponID } = route.params;
  const [_coupon, _setCoupon] = useState(null);
  const [_isLoading, _setLoading] = useState(true);

  const [_getCoupon] = useLazyQuery(GET_COUPON, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getCoupon == null) return;
      
      _setCoupon(data?.getCoupon);
    }
  });

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      _getCoupon({
        variables: {couponID},
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
    });

    return unsubscribe;
  }, [navigation])


  if (_isLoading) {
    return (
      <Container>
        <ActivityIndicator color={colors.primaryColor} />
      </Container>
    );
  }

  return (
    <Container disablePadding>
      <ScrollView showsVerticalScrollIndicator={false}>
      <View
          style={{
            position: 'relative',
            width: '100%',
            height: 300,
          }}>
          <LinearGradient
            colors={[
              'rgba(0, 0, 0, 0)',
              'rgba(0, 0, 0, 0.2)',
              'rgba(0, 0, 0, 0.5)',
            ]}
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              zIndex: 2,
            }}
          />
          <Image
            style={{
              width: '100%',
              height: '100%',
            }}
            resizeMode="cover"
            source={{
              uri: _coupon.photo,
            }}
          />
        </View>
        <View
          style={{
            alignItems: 'center',
            backgroundColor: colors.whiteColor,
            paddingBottom: 15,
            marginBottom: 10,
            borderBottomWidth: 1,
            borderColor: colors.inputBackgroundColor,
          }}>
          <H1 style={{ marginTop: 5, marginBottom: 5 }}>{_coupon.name}</H1>
          <Row alignCenter>
            <Icon
              name="refresh-ccw"
              size={20}
              style={{marginRight: 10}}
            />
            <P style={{marginRight: 20}}>ใช้แล้ว {_coupon.redeems} ครั้ง</P>
          </Row>
        </View>
        <Panel
          title="รายละเอียด"
          description={_coupon.description}
        />
        <Panel
          title="เงื่อนไข"
          description={_coupon.condition}
        />
      </ScrollView>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
})

export default connect(mapStateToProps)(CouponDetailScreen);
