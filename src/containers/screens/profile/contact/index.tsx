import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@components/appbars/AppbarBack';
import ContactScreen from './ContactScreen';
import FrequentQuestionScreen from './FrequentQuestionScreen';
import FrequentQuestionSingleScreen from './FrequentQuestionSingleScreen';
import ContactSubmitScreen from './ContactSubmitScreen';
import ContactReceiptScreen from './ContactReceiptScreen';

const Stack = createStackNavigator();

const ContactStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="ContactScreen"
      headerMode="screen"
    >
      <Stack.Screen
        name="ContactScreen"
        component={ContactScreen}
        options={{
          header: props => <AppbarBack title="ติดต่อเรา" {...props} />,
        }}
      />
      <Stack.Screen
        name="FrequentQuestionScreen"
        component={FrequentQuestionScreen}
        options={{
          header: props => <AppbarBack title="คำถามพบบ่อย" {...props} />,
        }}
      />
      <Stack.Screen
        name="FrequentQuestionSingleScreen"
        component={FrequentQuestionSingleScreen}
        options={{
          header: props => <AppbarBack title="คำถามพบบ่อย" {...props} />,
        }}
      />
      <Stack.Screen
        name="ContactSubmitScreen"
        component={ContactSubmitScreen}
        options={{
          header: props => <AppbarBack title="ติดต่อเรา" {...props} />,
        }}
      />
      <Stack.Screen
        name="ContactReceiptScreen"
        component={ContactReceiptScreen}
        options={{
          header: props => <AppbarBack title="ติดต่อเรา" {...props} />,
        }}
      />
    </Stack.Navigator>
  );
}

export default ContactStack