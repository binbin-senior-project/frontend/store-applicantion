import React from 'react';
import {StatusBar} from 'react-native';
import {
  Container,
  Content,
  Row,
  Col,
  Footer,
  H1,
  H3,
  P,
} from '@components/Global';
import Logo from '@components/logos/Logo';
import Divider from '@components/Divider';
import Button from '@components/buttons/Button';

interface Props {
  navigation: any;
}

const ContactReceiptScreen = ({navigation}: Props) => (
  <Container>
    <Content style={{justifyContent: 'center'}}>
      <Col center>
        <Logo width={100} />
        <H1>สำเร็จ</H1>
        <H3>ส่งคำถามเรียบร้อย</H3>
      </Col>
      <Divider />
      <Row between>
        <Content>
          <P bold>เลขรายการ</P>
          <P>912532411500775</P>
        </Content>
        <Content>
          <P bold right>
            ประเภทคำถาม
          </P>
          <P right>ปัญหาด้านคูปอง</P>
        </Content>
      </Row>
      <Divider />
      <Row between>
        <Content>
          <P bold>วันที่</P>
          <P>20 มิถุนายน 2562</P>
        </Content>
        <Content>
          <P bold right>
            เวลา
          </P>
          <P right>11:26</P>
        </Content>
      </Row>
      <Divider />
    </Content>
    <Footer>
      <Button
        text="เรียบร้อย"
        onPress={() => navigation.navigate('ProfileScreen')}
      />
    </Footer>
  </Container>
);

export default ContactReceiptScreen;
