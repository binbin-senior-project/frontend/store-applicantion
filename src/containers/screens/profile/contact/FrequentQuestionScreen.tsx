import React from 'react'
import { ScrollView } from 'react-native';
import {
  Container,
  Center,
  Row,
  H3,
} from '@components/Global';
import CardButton from '@components/buttons/CardButton';

interface Props {
  navigation: any,
}

const FrequentQuestionScreen = ({ navigation }: Props) => {
  return (
    <ScrollView>
      <Container>
        <CardButton
          text="ไม่สามารถสร้างคูปองได้"
          onPress={() => navigation.navigate('FrequentQuestionSingleScreen')}
        />
      </Container>
    </ScrollView>
  );
}

export default FrequentQuestionScreen;