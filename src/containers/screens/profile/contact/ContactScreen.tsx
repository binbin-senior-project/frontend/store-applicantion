import React from 'react'
import {
  Container,
  Center,
  Row,
  H3,
} from '@components/Global';
import Logo from '@components/logos/Logo';
import Button from '@components/buttons/Button';
import OutlinedButton from '@components/buttons/OutlinedButton';

interface Props {
  navigation: any,
}

const ContactScreen = ({ navigation }: Props) => {
  return (
    <Container>
      <Center>
        <Row style={{ marginBottom: 40 }}>
          <Logo />
        </Row>
        <H3>ต้องการความช่วยเหลือ ให้คำแนะนำ</H3>
        <H3>หรือติดต่อสอบถาม</H3>
      </Center>
      <Button
        text="คำถามที่พบบ่อย"
        onPress={() => navigation.navigate('FrequentQuestionScreen')}
      />
      <OutlinedButton
        text="ติดต่อสอบถาม"
        onPress={() => navigation.navigate('ContactSubmitScreen')}
      />
    </Container>
  );
}

export default ContactScreen;