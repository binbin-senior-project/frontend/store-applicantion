import React from 'react'
import { View } from 'react-native';
import {
  Container,
  Center,
  Row,
  H3,
} from '@components/Global';
import Panel from '@components/panels/Panel';
import OutlinedButton from '@components/buttons/OutlinedButton';

const FrequentQuestionSingleScreen = () => {
  return (
    <Container noPadding>
      <Panel
        title="ไม่สามารถแลกคูปองได้"
        description="ปัญหาที่พบส่วนมากที่พบจะมาจากการไม่มีแต้มเพียงพอในบัญชี เพื่อที่จะใช้ในการแลกแต้ม"
      />
      <Panel
        title="ได้ประโยชน์จากคำตอบหรือไม่?"
      >
        <View style={{ padding: 20 }}>
          <OutlinedButton
            text="มีประโยชน์"
            small
          />
          <OutlinedButton
            text="ไม่มีประโยชน์"
            small
            grey
          />
        </View>
      </Panel>
    </Container>
  );
}

export default FrequentQuestionSingleScreen;