import React, {useState, useEffect} from 'react';
import {StatusBar, Modal, SafeAreaView} from 'react-native';
import {connect} from 'react-redux';
import uuid from 'react-native-uuid';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import Panel from '@components/panels/Panel';
import PanelTransparent from '@components/panels/PanelTransparent';
import Button from '@components/buttons/Button';
import Input from '@components/forms/Input';
import Select from '@components/forms/Select';
import Radio from '@components/forms/Radio';
import UploadButton from '@components/buttons/UploadButton';
import LoaderDefault from '@components/loaders/LoaderDefault';
import Error from '@components/Error';
import {Row, Container, Footer, Content} from '@components/Global';
import {UPDATE_STORE} from '@graphql/mutation';
import {GET_STORE_CATEGORIES} from '@graphql/query';
import {useLazyQuery, useMutation} from '@apollo/react-hooks';
import { setCurrentStoreProfileAction } from '@actions/store';
import validator from 'validator';

interface Props {
  accessToken: string;
  store: any;
  setCurrentStoreProfile: Function;
  navigation: any;
}

const ProfileSettingScreen = ({accessToken, store, setCurrentStoreProfile, navigation}: Props) => {
  const [_storeLogo, _setStoreLogo] = useState(store.logo);
  const [_storeName, _setStoreName] = useState(store.name);
  const [_storeTagline, _setStoreTagline] = useState(store.tagline);
  const [_storePhone, _setStorePhone] = useState(store.phone);
  const [_storeLatitude] = useState(store.latitude);
  const [_storeLongitude] = useState(store.longitude);
  const [_storeCategory, _setStoreCategory] = useState(store.category);
  const [_storeCategories, _setStoreCategories] = useState([]);
  const [_isVisible, _setVisible] = useState(false);
  const [_isLoading, _setLoading] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');

  const [_updateStore] = useMutation(UPDATE_STORE, {
    onCompleted: (data) => {
      _setLoading(false);

      if (data?.updateStore.success) {
        setCurrentStoreProfile();
      }
    }
  });

  // Get Categories
  const [_getStoreCategories] = useLazyQuery(GET_STORE_CATEGORIES, {
    onCompleted: (data: any) => {
      if (data?.getStoreCategories == null) return;

      const newStoreCategories = data?.getStoreCategories?.map((item: any) => {
        return {
          id: item.id,
          name: item.name,
        };
      });

      _setStoreCategories([..._storeCategories, ...newStoreCategories]);
    },
  });

  const onSelectCategory = (category: any) => {
    _setStoreCategory(category);
    _setVisible(false);
  };

  const onSelectImage = () => {
    const options = {
      title: 'เลือกรูปภาพ',
      maxWidth: 300,
      maxHeight: 300,
    }

    ImagePicker.showImagePicker(options, (response: any) => {
      if (response.didCancel || response.error) {
        return;
      }

      _setStoreLogo(response.uri);
    });
  };

  const onSubmit = async () => {

    if (validator.isEmpty(_storeLogo)) {
      _setError(true);
      _setErrorMsg('โปรดอัปโหลดโลโก้ร้าน');
      return;
    }

    if (validator.isEmpty(_storeName)) {
      _setError(true);
      _setErrorMsg('ระบุชื่อร้านให้ถูกต้อง');
      return;
    }

    if (validator.isEmpty(_storeTagline)) {
      _setError(true);
      _setErrorMsg('ระบุแท็กไลน์ให้ถูกต้อง');
      return;
    }

    if (!validator.isMobilePhone(_storePhone, 'th-TH')) {
      _setError(true);
      _setErrorMsg('ระบุมือถือให้ถูกต้อง');
      return;
    }

    if (!_storeCategory) {
      _setError(true);
      _setErrorMsg('ระบุหมวดหมู่ร้าน');
      return;
    }

    _setLoading(true);

    let logo = _storeLogo;

    if (_storeLogo != '' && !_storeLogo.includes('https')) {
      const photoName = uuid.v4();
      const reference = storage().ref(photoName);
      await reference.putFile(_storeLogo);

      logo = await storage().ref(photoName).getDownloadURL();
    }

    _updateStore({
      variables: { input: {
        storeID: store.id,
        logo: logo,
        name: _storeName,
        tagline: _storeTagline,
        phone: _storePhone,
        latitude: _storeLatitude,
        longitude: _storeLongitude,
        categoryID: _storeCategory.id,
      } },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  };

  useEffect(() => {
    _getStoreCategories({
      variables: {offset: 0, limit: 100},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  }, [navigation]);

  return (
    <Container>
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Modal visible={_isVisible} animationType="slide">
        <SafeAreaView>
          <StatusBar barStyle="dark-content" />
          <Panel title="เลือกประเภทร้านค้า">
            {_storeCategories?.map((item: any) => (
              <Radio
                key={item.id}
                title={item.name}
                onPress={() => onSelectCategory(item)}
              />
            ))}
          </Panel>
        </SafeAreaView>
      </Modal>
      <Content>
        <PanelTransparent title="ข้อมูลกิจการ">
          <Row center style={{ marginTop: 40 }}>
            <UploadButton
              avatar={_storeLogo}
              onPress={() => onSelectImage()}
            />
          </Row>
          <Input
            placeholder="ชื่อร้าน"
            value={_storeName}
            onChangeText={(text: string) => _setStoreName(text)}
          />
          <Input
            placeholder="แท็กไลน์"
            value={_storeTagline}
            onChangeText={(text: string) => _setStoreTagline(text)}
          />
          <Input
            placeholder="เบอร์ติดต่อ"
            value={_storePhone}
            onChangeText={(text: string) => _setStorePhone(text)}
          />
          <Select
            placeholder="ประเภทร้าน"
            onPress={() => _setVisible(true)}
            value={_storeCategory.name}
          />
        </PanelTransparent>
      </Content>
      <Footer>
        <Button text="บันทึกข้อมูล" onPress={() => onSubmit()} />
      </Footer>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  store: state.store,
});

const mapDispatchToProps = (dispatch: Function) => ({
  setCurrentStoreProfile: () => dispatch(setCurrentStoreProfileAction()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettingScreen);
