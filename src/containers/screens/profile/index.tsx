import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import AppbarBack from '@components/appbars/AppbarBack';
import ProfileScreen from './ProfileScreen';
import ProfileSettingScreen from './setting/SettingScreen';
import PaymentSetting from './payment/PaymentSettingScreen';
import ContactStack from './contact';

const Stack = createStackNavigator();

const ProfileStack = () => {
  return (
    <Stack.Navigator initialRouteName="ProfileScreen" headerMode="screen">
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          header: props => <AppbarDefault title="โปรไฟล์" {...props} />,
        }}
      />
      <Stack.Screen
        name="ProfileSettingScreen"
        component={ProfileSettingScreen}
        options={{
          header: props => <AppbarBack title="ตั้งค่าโปรไฟล์" {...props} />,
        }}
      />
      <Stack.Screen
        name="PaymentSetting"
        component={PaymentSetting}
        options={{
          header: props => <AppbarBack title="ตั้งค่าการชำระเงิน" {...props} />,
        }}
      />
      <Stack.Screen
        name="ContactStack"
        component={ContactStack}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default ProfileStack;
