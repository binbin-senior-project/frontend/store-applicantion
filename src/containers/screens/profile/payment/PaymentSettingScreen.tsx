import React, { useState } from 'react';
import { ScrollView, Modal } from 'react-native';
import { Container, Row } from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import HighlightButton from '@components/buttons/HighlightButton';
import AddPayment from '@components/modals/AddPayment';
import PaymentCard from '@components/cards/PaymentCard';

const PaymentSettingScreen = () => {
  const [isAddPayment, setAddPayment] = useState(false);

  const onComplete = () => {
    setAddPayment(false);
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container>
        <Modal
          animationType="slide"
          visible={isAddPayment}
        >
          <AddPayment
            onHideModal={() => setAddPayment(false)}
            onComplete={onComplete}
          />
        </Modal>
        <PanelTransparent title="การชำระเงิน">
          <PaymentCard
            name="บัตรเดรบิต xxx-2442-xxx"
            selected
          />
          <Row end style={{ paddingTop: 20 }}>
            <HighlightButton
              text="เพิ่มบัตรเครดิต/เดบิต"
              onPress={() => setAddPayment(true)}
            />
          </Row>
        </PanelTransparent>
      </Container>
    </ScrollView>
  )
}

export default PaymentSettingScreen;