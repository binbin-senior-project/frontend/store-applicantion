import React from 'react';
import {View, ScrollView} from 'react-native';
import {connect} from 'react-redux';
import HighlightButton from '@components/buttons/HighlightButton';
import Panel from '@components/panels/Panel';
import ListItem from '@components/ListItem';
import {Container} from '@components/Global';
import colors from '@utils/colors';
import StoreCard from '@containers/StoreCard';
import {logoutAction} from '@actions/auth';

interface Props {
  logout: Function;
  navigation: any;
}

const ProfileScreen = ({logout, navigation}: Props) => {
  const onLogout = () => {
    logout();

    navigation.reset({
      index: 0,
      routes: [{name: 'RegisterStack'}],
    });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container disablePadding>
        <StoreCard />
        <Panel>
          <ListItem
            icon="settings"
            title="ตั้งค่าโปรไฟล์"
            onPress={() => {
              navigation.push('ProfileSettingScreen');
            }}
          />
        </Panel>
        <Panel>
          <View
            style={{
              padding: 20,
              borderBottomWidth: 1,
              borderBottomColor: colors.grayBorder,
            }}>
            <HighlightButton onPress={onLogout} text="ออกจากระบบ" />
          </View>
        </Panel>
      </Container>
    </ScrollView>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  logout: () => dispatch(logoutAction()),
});

export default connect(
  null,
  mapDispatchToProps,
)(ProfileScreen);
