import React, {useState, useEffect} from 'react';
import {RefreshControl, FlatList} from 'react-native';
import {connect} from 'react-redux';
import {useLazyQuery} from '@apollo/react-hooks';
import NotificationItem from '@components/NotificationItem';
import NoData from '@components/NoData';
import {GET_TRANSACTIONS} from '@graphql/query';
import colors from '@utils/colors';
import {resetNotificationAction} from '@actions/notification';
import {Container, Center} from '@components/Global';
interface Props {
  storeID: string;
  accessToken: string;
  resetNotification: Function;
  navigation: any;
}

const TransactionScreen = ({
  storeID,
  accessToken,
  resetNotification,
  navigation,
}: Props) => {
  const [_transactions, _setTransactions] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [_isLoading, _setLoading] = useState(false);

  const [_getTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data.getTransactions == null) return;
      _setTransactions(data.getTransactions);
      _setOffset(0);
    },
  });

  const [_loadMoreTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data.getTransactions == null) return;
      _setTransactions([..._transactions, ...data.getTransactions]);
    },
  });

  const onRefresh = () => {
    _getTransactions({
      variables: {storeID: storeID, offset: 0, limit: 12},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  };

  const onEndReached = () => {
    _setOffset(_offset + 12);
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      resetNotification();

      _getTransactions({
        variables: {storeID: storeID, offset: 0, limit: 12},
        context: {headers: {authorization: `Bearer ${accessToken}`}},
      });
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (_offset == 0) return;

    _loadMoreTransactions({
      variables: {storeID: storeID, offset: _offset, limit: 12},
      context: {headers: {authorization: `Bearer ${accessToken}`}},
    });
  }, [_offset]);

  return (
    <>
      {_transactions.length == 0 ? (
        <Container>
          <Center>
            <NoData title="ไม่มีการทำรายการ" />
          </Center>
        </Container>
      ) : (
        <FlatList
          data={_transactions}
          refreshControl={
            <RefreshControl
              refreshing={_isLoading}
              onRefresh={onRefresh}
              tintColor={colors.primaryColor}
            />
          }
          onEndReached={() => onEndReached()}
          onEndReachedThreshold={1}
          numColumns={1}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}: {item: any; index: number}) => {
            return (
              <NotificationItem
                key={item.id}
                id={item.id}
                description={item.description}
                createdAt={item.createdAt}
                onPress={() => {}}
              />
            );
          }}
          keyExtractor={item => item.id}
          contentContainerStyle={{
            marginVertical: 10,
            paddingBottom: 10,
          }}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: any) => ({
  storeID: state.store.id,
  accessToken: state.auth.accessToken,
});

const mapDispatchToProps = (dispatch: Function) => ({
  resetNotification: () => dispatch(resetNotificationAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransactionScreen);
