import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import NotificationScreen from '@containers/screens/notification/NotificationScreen';

const Stack = createStackNavigator();

const HomeStack = () => {
  return (
    <Stack.Navigator initialRouteName="NotificationScreen" headerMode="screen">
      <Stack.Screen
        name="NotificationScreen"
        component={NotificationScreen}
        options={{
          header: props => <AppbarDefault title="การแจ้งเตือน" {...props} />,
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
