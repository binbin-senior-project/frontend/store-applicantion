import React, {useState, useEffect} from 'react';
import {Image, TouchableWithoutFeedback, View} from 'react-native';
import {withNavigation} from '@react-navigation/compat';
import Coin from '@components/Coin';
import {Row, Col, Span, H2, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  id: string;
  name: string;
  image: string;
  expire: string;
  point: string;
  status: string;
  navigation: any;
}

const CouponItem = ({
  id,
  name,
  image,
  point,
  expire,
  status,
  navigation,
}: Props) => {
  let [_statusColor, _setStatusColor] = useState('');
  let [_statusDisplay, _setStatusDisplay] = useState('');

  useEffect(() => {
    switch (status) {
      case 'pending':
        _setStatusDisplay('รอการอนุมัติ');
        _setStatusColor('#d1d1d1');
        break;
      case 'active':
        _setStatusDisplay('กำลังใช้งาน');
        _setStatusColor('#04B11B');
        break;
      case 'rejected':
        _setStatusDisplay('ไม่ผ่านการอนุมัติ');
        _setStatusColor('#ff5555');
        break;
      case 'expired':
        _setStatusDisplay('หมดอายุ');
        _setStatusColor('#d1d1d1');
        break;
      default:
        _setStatusDisplay('');
    }
  }, [status]);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        navigation.navigate('CouponDetailScreen', {couponID: id});
      }}>
      <View
        style={{
          paddingVertical: 15,
          paddingHorizontal: 20,
          borderRadius: 5,
          backgroundColor: colors.whiteColor,
          marginBottom: 10,
          borderRightWidth: 1,
          borderRightColor: colors.inputBackgroundColor,
          borderBottomWidth: 2,
          borderBottomColor: colors.inputBackgroundColor,
        }}>
        <Row alignCenter>
          <Col style={{paddingRight: 20}}>
            <Image
              source={{uri: image}}
              style={{
                width: 100,
                height: 110,
                borderRadius: 5,
              }}
            />
          </Col>
          <Col style={{flex: 1}}>
            <H2 style={{marginTop: 5}}>{name}</H2>
            <Span>{`หมดอายุ ${expire}`}</Span>
            <Row style={{marginTop: 10}} between alignCenter>
              <Row alignCenter>
                <View
                  style={{
                    width: 10,
                    height: 10,
                    backgroundColor: _statusColor,
                    borderRadius: 10,
                    marginRight: 5,
                  }}
                />
                <Span>{_statusDisplay}</Span>
              </Row>
              <Row alignCenter>
                <P bold style={{marginRight: 5}}>
                  {point}
                </P>
                <Coin />
              </Row>
            </Row>
          </Col>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default withNavigation(CouponItem);
