import React, {useState, useRef, useEffect} from 'react';
import {Modal, StatusBar, Button, View} from 'react-native';
import {withNavigation} from '@react-navigation/compat';
import MapView, {Marker} from 'react-native-maps';
import Icon from 'react-native-vector-icons/Feather';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {Container, Footer, Content} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  onDone: Function;
  visible?: boolean;
  navigation: any;
}

const MapModal = ({onDone, visible, navigation}: Props) => {
  const [_latitude, _setLatitude] = useState(0);
  const [_longitude, _setLongitude] = useState(0);
  const map = useRef(null);

  const onRegionChange = (region: any) => {
    _setLatitude(region.latitude);
    _setLongitude(region.longitude);
  };

  useEffect(() => {
    // Get Current Location
    // _setLatitude();
    // _setLongitude();
  }, [navigation]);

  return (
    <Modal visible={visible}>
      <StatusBar barStyle="dark-content" />
      <Container style={{paddingTop: 60}}>
        <Icon
          name="close"
          size={35}
          onPress={() => navigation.pop()}
          style={{marginBottom: 10}}
        />
        <Content>
          <GooglePlacesAutocomplete
            placeholder="Enter Location"
            minLength={2}
            autoFocus={false}
            returnKeyType={'search'}
            fetchDetails={true}
            styles={{
              textInputContainer: {
                backgroundColor: 'rgba(0,0,0,0)',
                borderTopWidth: 0,
                borderBottomWidth: 0,
              },
              textInput: {
                marginLeft: 0,
                marginRight: 0,
                height: 38,
                color: '#5d5d5d',
                fontSize: 16,
              },
              predefinedPlacesDescription: {
                color: '#1faadb',
              },
            }}
            currentLocation={false}
            onPress={(data, details = null) => {
              _setLatitude(details.geometry.location.lat);
              _setLongitude(details.geometry.location.lng);
            }}
            query={{
              key: 'AIzaSyCxVJ1xBm4zONHDpyiZPWrKVmg3xQsm3B0',
              language: 'th',
            }}
          />
          <MapView
            ref={map}
            initialRegion={{
              latitude: _latitude,
              longitude: _longitude,
              latitudeDelta: 0.0222,
              longitudeDelta: 0.0221,
            }}
            style={{
              width: '100%',
              height: '90%',
            }}
            onRegionChange={onRegionChange}
            showsUserLocation
          />
          <Marker
            coordinate={{
              latitude: _latitude,
              longitude: _longitude,
            }}>
            <View
              style={{
                position: 'relative',
                backgroundColor: colors.primaryColor,
                width: 20,
                height: 20,
                borderRadius: 50,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  backgroundColor: 'rgba(62, 157, 26, 0.2)',
                  width: 50,
                  height: 50,
                  borderRadius: 50,
                }}
              />
            </View>
          </Marker>
        </Content>
        <Footer>
          <Button
            title="เรียบร้อย"
            onPress={() => onDone(_latitude, _longitude)}
          />
        </Footer>
      </Container>
    </Modal>
  );
};

export default withNavigation(MapModal);
