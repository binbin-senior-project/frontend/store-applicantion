import React from 'react'
import { View, Text, StatusBar, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundColor,
    height: '100%',
    padding: 20,
    paddingTop: 100,
    position: 'relative'
  },
  contentHolder: {
    flex: 1,
  },
  buttonHolder: {
    height: 100,
  },
  close: {
    position: 'absolute',
    top: 80,
    right: 20,
  },
  header: {
    ...fonts.h1BoldBlack,
  },
  paragraph: {
    ...fonts.h3,
  },
})

const AddPayment = ({ onHideModal, onComplete }) => {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Icon
        name="x"
        size={30}
        style={styles.close}
        onPress={() => onHideModal()}
      />
      <View style={styles.contentHolder}>
        <Text style={styles.header}>
          เพิ่มข้อมูลบัตร
        </Text>
        <Text style={[styles.paragraph, { marginBottom: 20 }]}>
          ชำระเงินผ่านบัตรเครดิต / เดบิต
        </Text>
        <Input icon="user" placeholder="ชื่อบนบัตร" />
        <Input icon="credit-card" placeholder="เลขบนบัตร" />
        <Input icon="calendar" placeholder="เดือน / ปีหมดอายุ" />
        <Input icon="lock" placeholder="CVV" />
      </View>
      <View style={styles.buttonHolder}>
        <Button
          text="เพิ่มบัตร"
          onPress={onComplete}
        />
      </View>
    </View>
  );
}

export default AddPayment;