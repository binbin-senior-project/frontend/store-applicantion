import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, StatusBar} from 'react-native';
import TouchID from 'react-native-touch-id';
import Icon from 'react-native-vector-icons/Feather';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import Pincode from '@components/Passcode';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.backgroundColor,
    height: '100%',
    padding: 20,
    paddingTop: 100,
    position: 'relative',
  },
  contentHolder: {
    flex: 1,
  },
  close: {
    position: 'absolute',
    top: 80,
    right: 20,
  },
  header: {
    ...fonts.h1BoldBlack,
  },
  paragraph: {
    ...fonts.h3,
  },
});

const VerifyModal = ({onHideModal, onComplete}) => {
  const [pincode, setPincode] = useState([]);
  const [isTouchID, setTouchID] = useState(true);

  useEffect(() => {
    if (isTouchID) {
      TouchID.authenticate('Verify Your Passcode')
        .then(() => onComplete())
        .catch(error => {
          const result = error.toString();
          const isCancel = result.match('LAErrorUserCancel');
          const isFallback = result.match('LAErrorUserFallback');

          if (isCancel || isFallback) {
            setTouchID(false);
          }
        });
    }
  });

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" />
      <Icon
        name="x"
        size={30}
        style={styles.close}
        onPress={() => onHideModal()}
      />
      <View style={styles.contentHolder}>
        <Text style={styles.header}>ยืนยันรหัส PIN</Text>
        <Text style={[styles.paragraph, {marginBottom: 20}]}>
          ปลดล็อคการทำรายการ
        </Text>
        <Pincode
          pincode={pincode}
          setPincode={setPincode}
          onComplete={onComplete}
        />
      </View>
    </View>
  );
};

export default VerifyModal;
