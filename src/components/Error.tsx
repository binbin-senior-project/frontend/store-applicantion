import React, { useEffect, useRef } from 'react';
import {
  TouchableWithoutFeedback,
  View,
  Modal,
  Animated,
  Vibration,
} from 'react-native';
import Divider from '@components/Divider';
import { H3, H2, Col, Row, P } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  message: string;
  isError?: boolean;
  onPress: Function;
}

const Error = ({ message, isError, onPress }: Props) => {
  useEffect(() => {
    if (isError) {
      Vibration.vibrate(1);
    }
  }, [isError]);

  return (
    <Modal animationType="none" transparent={true} visible={isError}>
      <View
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.2)',
          height: '100%',
        }}>
        <Animated.View
          style={{
            width: '85%',
            padding: 20,
            borderRadius: 10,
            backgroundColor: colors.whiteColor,
          }}>
          <Col>
            <H3 bold>เกิดข้อผิดพลาด</H3>
            <Divider />
            <P>{message}</P>
            <TouchableWithoutFeedback onPress={() => onPress()}>
              <Row
                center
                style={{
                  paddingVertical: 7,
                  borderRadius: 30,
                  backgroundColor: colors.primaryColor,
                  marginTop: 30,
                  color: colors.whiteColor,
                }}>
                <H3 white>เข้าใจแล้ว</H3>
              </Row>
            </TouchableWithoutFeedback>
          </Col>
        </Animated.View>
      </View>
    </Modal>
  );
};

export default Error;
