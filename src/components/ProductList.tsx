import React from 'react';
import {View, FlatList, ActivityIndicator} from 'react-native';
import ProdcutItem from '@components/ProdcutItem';
import colors from '@utils/colors';

interface Props {
  isLoading: boolean;
  scrollEnabled?: boolean;
  products: any;
  onItemPress: Function;
  onEndReached: Function;
}

const ProductList = ({
  isLoading,
  scrollEnabled,
  products,
  onItemPress,
  onEndReached,
}: Props) => {
  return (
    <>
      {isLoading && (
        <ActivityIndicator
          color={colors.primaryColor}
          style={{marginVertical: 40}}
        />
      )}
      <FlatList
        scrollEnabled={scrollEnabled}
        data={products}
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={1}
        numColumns={1}
        renderItem={({item, index}: {item: any; index: number}) => {
          return (
            <>
              <ProdcutItem
                key={item.id}
                id={item.id}
                name={item.name}
                price={item.price}
                checked={item.checked}
                onItemPress={() => onItemPress(item)}
              />
              {products.length - 1 != index && (
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: colors.grayBorder,
                  }}
                />
              )}
            </>
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          backgroundColor: colors.whiteColor,
          marginTop: 10,
        }}
      />
    </>
  );
};

export default ProductList;
