import React from 'react';
import {Image} from 'react-native';
import {Container, Center, H2} from '@components/Global';
import NoDataImage from '@images/no_data.png';

interface Props {
  title: string;
}

const NoData = ({title}: Props) => {
  return (
    <Container style={{backgroundColor: 'transparent'}}>
      <Center>
        <Image source={NoDataImage} resizeMode="contain" />
        <H2 style={{color: '#dddddd', marginTop: 20}}>{title}</H2>
      </Center>
    </Container>
  );
};

export default NoData;
