import styled from 'styled-components';
import colors from '@utils/colors';

export const Container = styled.View`
  padding: ${(props: {disablePadding: any}) =>
    props.disablePadding ? '0px' : '20px'}
  background-color: ${colors.pageBackgroundColor};
  height: 100%;
`;

// Reusable Component
export const Center = styled.View`
  flex: ${(props: {disableFlex?: number}) => (props.disableFlex ? 'none' : 1)};
  justify-content: center;
  align-items: center;
`;

export const Content = styled.View`
  flex: 1;
`;

export const Footer = styled.View`
  height: 80px;
`;

// Layout
export const Row = styled.View`
  flex-direction: row;
  ${(props: {alignCenter: boolean}) =>
    props.alignCenter && 'align-items: center;'}
  ${(props: {center: boolean}) => props.center && 'justify-content: center;'}
  ${(props: {between: boolean}) =>
    props.between && 'justify-content: space-between;'}
  ${(props: {end: boolean}) => props.end && 'justify-content: flex-end;'}
`;

export const Col = styled.View`
  flex-direction: column;
  ${(props: {center: boolean}) => props.center && 'align-items: center;'}
`;

export const Card = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background-color: ${colors.whiteColor};
  padding: 20px 15px;
  margin-bottom: 10;
  border-radius: 5;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.05);
`;

// Typhography
export const Font = styled.Text`
  font-family: DBHeavent;
  font-weight: normal;
  font-style: normal;
  letter-spacing: 0;
  font-weight: ${(props: {bold: boolean}) => (props.bold ? 'bold' : 'normal')}
  color: ${(props: {green: boolean; white: boolean}) => {
    if (props.green) return colors.primaryColor;
    if (props.white) return colors.whiteColor;
    return colors.fontColor;
  }}
  text-align: ${(props: {center: boolean; right: boolean}) => {
    if (props.right) return 'right';
    if (props.center) return 'center';
    return 'left';
  }}
`;

export const H1 = styled(Font)`
  font-weight: bold;
  font-size: 40px;
`;

export const H2 = styled(Font)`
  font-size: 30px;
`;

export const H3 = styled(Font)`
  font-size: 27px;
`;

export const P = styled(Font)`
  font-size: 25px;
`;

export const Span = styled(P)`
  font-size: 20px;
`;
