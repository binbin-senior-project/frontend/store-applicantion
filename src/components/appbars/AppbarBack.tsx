import React from 'react';
import {SafeAreaView} from 'react-native';
import {H1, Row} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';

interface Props {
  title: string;
  navigation: any;
}

const AppbarBack = ({title, navigation}: Props) => {
  return (
    <Row
      alignCenter
      style={{
        paddingHorizontal: 20,
        paddingTop: 45,
        paddingBottom: 10,
        backgroundColor: colors.primaryColor,
      }}>
      <Icon
        name="arrow-left"
        size={25}
        color={colors.whiteColor}
        style={{marginRight: 20}}
        onPress={() => navigation.goBack()}
      />
      <H1 white>{title}</H1>
    </Row>
  );
};

export default AppbarBack;
