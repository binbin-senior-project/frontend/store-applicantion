import React from 'react';
import {View, TouchableWithoutFeedback} from 'react-native';
import {Row, H1, P} from '@components/Global';
import AvatarButton from '@components/buttons/AvatarButton';
import colors from '@utils/colors';

interface Props {
  title: string;
  navigation: any;
}

const AppbarCoupon = ({title, navigation}: Props) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingTop: 45,
        paddingBottom: 10,
        backgroundColor: colors.primaryColor,
      }}>
      <Row center alignCenter>
        <Row style={{marginRight: 20}}>
          <AvatarButton onPress={() => navigation.navigate('ProfileScreen')} />
        </Row>
        <H1 white style={{margin: 10}}>
          {title}
        </H1>
      </Row>
      <TouchableWithoutFeedback onPress={() => navigation.push('EditCoupon')}>
        <View
          style={{
            backgroundColor: colors.secondaryColor,
            borderRadius: 40,
            paddingHorizontal: 25,
            paddingVertical: 5,
          }}>
          <P white>แก้ไขคูปอง</P>
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default AppbarCoupon;
