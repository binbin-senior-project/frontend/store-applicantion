import React from 'react';
import {View, Text, TouchableWithoutFeedback} from 'react-native';
import {H1, Row} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import AvatarButton from '@components/buttons/AvatarButton';
import colors from '@utils/colors';

interface Props {
  title: string;
  navigation: any;
}

const AppbarWithBell = ({title, navigation}: Props) => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingTop: 45,
        paddingBottom: 10,
        backgroundColor: colors.primaryColor,
      }}>
      <Row center alignCenter>
        <Row style={{marginRight: 20}}>
          <AvatarButton onPress={() => navigation.navigate('ProfileScreen')} />
        </Row>
        <H1 white>{title}</H1>
      </Row>
      <TouchableWithoutFeedback
        onPress={() => navigation.push('TransactionScreen')}>
        <View>
          <View
            style={{
              position: 'absolute',
              top: -7,
              right: -10,
              backgroundColor: colors.redColor,
              paddingVertical: 3,
              paddingHorizontal: 7,
              borderRadius: 50,
              zIndex: 2,
            }}>
            <Text style={{color: colors.whiteColor}}>3</Text>
          </View>
          <Icon name="bell" size={25} color={colors.whiteColor} />
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default AppbarWithBell;
