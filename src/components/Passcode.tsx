import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
} from 'react-native';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import {H1} from './Global';

const styles = StyleSheet.create({
  passcodeHolder: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 80,
  },
  passcode: {
    width: 17,
    height: 17,
    borderRadius: 17,
    borderWidth: 2,
    borderColor: colors.primaryColor,
    marginHorizontal: 10,
  },
  passcodeActive: {
    backgroundColor: colors.primaryColor,
  },
  numberHolder: {
    alignItems: 'center',
  },
  numbers: {
    width: Dimensions.get('screen').width / 3 - 20,
    paddingVertical: 20,
  },
  numberText: {
    ...fonts.h1Black,
    textAlign: 'center',
  },
  numberRowHolder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

interface Props {
  onComplete: Function;
}

const Passcode = ({onComplete}: Props) => {
  const [passcode, setPasscode] = useState([]);

  const onPress = (number: number) => {
    if (passcode.length <= 6) {
      setPasscode([...passcode, number]);
    }
  };

  useEffect(() => {
    if (passcode.length == 6) {
      onComplete(passcode.join(''));
    }
  }, [passcode]);

  const onDelete = () => {
    passcode.pop();
    setPasscode([...passcode]);
  };

  return (
    <>
      <View style={styles.passcodeHolder}>
        <View
          style={[
            styles.passcode,
            passcode[0] != null && styles.passcodeActive,
          ]}
        />
        <View
          style={[
            styles.passcode,
            passcode[1] != null && styles.passcodeActive,
          ]}
        />
        <View
          style={[
            styles.passcode,
            passcode[2] != null && styles.passcodeActive,
          ]}
        />
        <View
          style={[
            styles.passcode,
            passcode[3] != null && styles.passcodeActive,
          ]}
        />
        <View
          style={[
            styles.passcode,
            passcode[4] != null && styles.passcodeActive,
          ]}
        />
        <View
          style={[
            styles.passcode,
            passcode[5] != null && styles.passcodeActive,
          ]}
        />
      </View>

      <View style={styles.numberHolder}>
        <View style={styles.numberRowHolder}>
          <TouchableWithoutFeedback onPress={() => onPress(1)}>
            <View style={styles.numbers}>
              <H1 center>1</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(2)}>
            <View style={styles.numbers}>
              <H1 center>2</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(3)}>
            <View style={styles.numbers}>
              <H1 center>3</H1>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View style={styles.numberRowHolder}>
          <TouchableWithoutFeedback onPress={() => onPress(4)}>
            <View style={styles.numbers}>
              <H1 center>4</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(5)}>
            <View style={styles.numbers}>
              <H1 center>5</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(6)}>
            <View style={styles.numbers}>
              <H1 center>6</H1>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View style={styles.numberRowHolder}>
          <TouchableWithoutFeedback onPress={() => onPress(7)}>
            <View style={styles.numbers}>
              <H1 center>7</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(8)}>
            <View style={styles.numbers}>
              <H1 center>8</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(9)}>
            <View style={styles.numbers}>
              <H1 center>9</H1>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <View style={styles.numberRowHolder}>
          <TouchableWithoutFeedback>
            <View style={styles.numbers}>
              <H1 center />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => onPress(0)}>
            <View style={styles.numbers}>
              <H1 center>0</H1>
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={onDelete}>
            <View style={styles.numbers}>
              <H1 center>ลบ</H1>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </>
  );
};

export default Passcode;
