import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 20,
    height: 20,
    borderRadius: 15,
    backgroundColor: '#ffd52d',
  },
  innerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 15,
    height: 15,
    borderRadius: 15,
    backgroundColor: '#d2a700',
  },
  icon: {
    fontFamily: 'DBHeavent',
    fontSize: 14,
    color: '#f9cd00',
  }
});

const Coin = () => (
  <View style={styles.container}>
    <View style={styles.innerContainer}>
      <Text style={styles.icon}>
        P
      </Text>
    </View>
  </View>
);

export default Coin;