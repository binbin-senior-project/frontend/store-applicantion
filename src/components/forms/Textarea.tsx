import React from 'react';
import { TextInput, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import fonts from '@utils/fonts';
import colors from '@utils/colors';

const styles = StyleSheet.create({
  textarea: {
    width: '100%',
    height: 150,
    padding: 20,
    paddingTop: 20,
    borderRadius: 5,
    backgroundColor: colors.inputBackgroundColor,
    marginTop: 5,
    marginBottom: 5,
    ...fonts.paragraph,
  },
});

const Textarea = ({ placeholder, innerRef }) => (
  <TextInput
    ref={innerRef}
    style={styles.textarea}
    placeholder={placeholder}
    placeholderTextColor={fonts.placeholder.color}
    multiline
  />
);

Textarea.propTypes = {
  placeholder: PropTypes.string.isRequired,
  innerRef: PropTypes.any.isRequired,
};

export default React.forwardRef((props, ref) => <Textarea innerRef={ref} {...props} />);
