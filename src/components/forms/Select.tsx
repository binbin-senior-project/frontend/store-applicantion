import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {Row, P} from '@components/Global';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

interface Props {
  placeholder: string;
  value: String;
  onPress: Function;
}

const Select = ({placeholder, value, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      alignCenter
      style={{
        borderRadius: 8,
        marginTop: 5,
        marginBottom: 5,
      }}>
      <Row
        between
        alignCenter
        style={{
          width: '100%',
          height: 60,
        }}>
        {value ? (
          <P>{value}</P>
        ) : (
          <P style={{color: fonts.placeholder.color}}>{placeholder}</P>
        )}
        <Icon name="chevron-down" size={20} color={fonts.placeholder.color} />
      </Row>
    </Row>
  </TouchableWithoutFeedback>
);

export default Select;
