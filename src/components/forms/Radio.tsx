import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import {Row, Col, P} from '@components/Global';
import {View} from 'react-native';
import colors from '@utils/colors';

interface Props {
  title: string;
  onPress: Function;
}

const Radio = ({title, onPress}: Props) => {
  return (
    <TouchableWithoutFeedback onPress={() => onPress()}>
      <View
        style={{
          padding: 20,
          borderBottomWidth: 1,
          borderBottomColor: colors.grayBorder,
        }}>
        <Row between alignCenter>
          <Col>
            <Row alignCenter>
              <P>{title}</P>
            </Row>
          </Col>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Radio;
