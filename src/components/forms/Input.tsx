import React, {useState, useEffect} from 'react';
import {TextInput} from 'react-native';
import validator from 'validator';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import {Row} from '@components/Global';

interface Props {
  placeholder: string;
  type?: string;
  value: string;
  onChangeText: Function;
}

const Input = ({placeholder, type, value, onChangeText}: Props) => {
  const [_lineColor, _setLineColor] = useState(colors.inputBackgroundColor);

  return (
    <Row
      style={{
        marginTop: 5,
        marginBottom: 5,
        borderBottomColor: _lineColor,
        borderBottomWidth: 2,
      }}>
      <TextInput
        style={{
          width: '100%',
          height: 60,
          fontFamily: 'DBHeavent',
          fontSize: 25,
          color: colors.fontColor,
        }}
        onFocus={() => _setLineColor(colors.primaryColor)}
        onBlur={() => _setLineColor(colors.inputBackgroundColor)}
        placeholder={placeholder}
        placeholderTextColor={fonts.placeholder.color}
        keyboardType={type === 'number' ? 'number-pad' : 'default'}
        secureTextEntry={type === 'password'}
        value={value}
        onChangeText={text => onChangeText(text)}
      />
    </Row>
  );
};

export default Input;
