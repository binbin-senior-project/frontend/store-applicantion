import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Row, Col, H3, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  id: string;
  name: string;
  price: number;
  checked: boolean;
  onItemPress: Function;
}

const ProdcutItem = ({id, name, price, checked, onItemPress}: Props) => {
  return (
    <TouchableWithoutFeedback onPress={() => onItemPress(id)}>
      <View style={{paddingVertical: 15, paddingHorizontal: 20}}>
        <Row alignCenter>
          <Col>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 25,
                height: 24,
                borderWidth: 2,
                borderColor: checked ? colors.primaryColor : colors.iconColor,
                marginRight: 15,
                borderRadius: 25,
              }}>
              {checked && (
                <View
                  style={{
                    width: 12,
                    height: 12,
                    borderRadius: 10,
                    backgroundColor: colors.primaryColor,
                  }}
                />
              )}
            </View>
          </Col>
          <Row alignCenter between style={{flex: 1}}>
            <P style={{marginTop: 5}}>{name}</P>
            <P>{price == 0 ? 'ฟรี' : price}</P>
          </Row>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ProdcutItem;
