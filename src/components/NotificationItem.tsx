import React from 'react';
import moment from 'moment';
import {Col, P, Span} from '@components/Global';
import {View} from 'react-native';
import colors from '@utils/colors';

interface Props {
  id: string;
  description: string;
  createdAt: string;
  onPress: Function;
}

const TransactionItem = ({id, description, createdAt, onPress}: Props) => {
  const d = Date.parse(createdAt.replace(/-/gi, '/').split('.')[0]);
  const format = moment(d).locale('th');
  createdAt = format.add(543, 'y').format('ll HH:mm');

  return (
    <View
      style={{
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: colors.grayBorder,
        backgroundColor: colors.whiteColor,
      }}>
      <Col>
        <P>{description}</P>
        <Span>{createdAt}</Span>
      </Col>
    </View>
  );
};

export default TransactionItem;
