import React from 'react';
import Dash from 'react-native-dash';
import colors from '@utils/colors';

interface Props {
  isWhite?: boolean;
}

const Divider = ({isWhite = false}: Props) => {
  const dashColor = isWhite ? colors.whiteBorder : colors.iconColor;

  return (
    <Dash
      dashColor={dashColor}
      dashGap={2}
      dashThickness={1}
      dashLength={3}
      style={{
        width: '100%',
        paddingVertical: 10,
      }}
    />
  );
};

export default Divider;
