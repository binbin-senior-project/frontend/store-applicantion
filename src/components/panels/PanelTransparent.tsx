import React, {ReactNode} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import Divider from '@components/Divider';
import fonts from '@utils/fonts';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginBottom: 15,
  },
  headerHolder: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  headerTitle: {
    ...fonts.h3Bold,
  },
  descriptionHolder: {
    padding: 20,
  },
  description: {
    ...fonts.paragraph,
  },
  buttonHolder: {
    ...fonts.paragraphGreen,
  },
});

interface PanelTransparentProps {
  title: string;
  description?: string;
  children: ReactNode;
  buttonText?: string;
  onPress?: Function;
}

const PanelTransparent = ({
  title,
  description,
  buttonText,
  onPress,
  children,
}: PanelTransparentProps) => (
  <View style={styles.container}>
    <View style={styles.headerHolder}>
      <Text style={styles.headerTitle}>{title}</Text>
      {!!onPress && (
        <TouchableOpacity onPress={onPress}>
          <Text style={styles.buttonHolder}>
            {buttonText ? buttonText : 'ดูทั้งหมด'}
          </Text>
        </TouchableOpacity>
      )}
    </View>
    <Divider />
    {!!children ? (
      children
    ) : (
      <View style={styles.descriptionHolder}>
        <Text style={styles.description}>{description}</Text>
      </View>
    )}
  </View>
);

export default PanelTransparent;
