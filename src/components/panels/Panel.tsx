import React, {ReactNode} from 'react';
import {View, TouchableOpacity} from 'react-native';
import colors from '@utils/colors';
import {H3, P} from '@components/Global';

interface PanelProps {
  title?: string;
  description?: string;
  children?: ReactNode;
  onPress?: Function;
}

const Panel = ({title, description, onPress, children}: PanelProps) => (
  <View
    style={{
      width: '100%',
      backgroundColor: colors.whiteColor,
      marginBottom: 10,
    }}>
    {!!title && (
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
          paddingVertical: 15,
          flexDirection: 'row',
          borderBottomWidth: 1,
          borderStyle: 'solid',
          borderColor: colors.whiteBorder,
          marginBottom: -5,
        }}>
        <H3 bold>{title}</H3>
        {!!onPress && (
          <TouchableOpacity
            onPress={() => {
              onPress();
            }}>
            <P green>ดูทั้งหมด</P>
          </TouchableOpacity>
        )}
      </View>
    )}
    {!!children ? (
      children
    ) : (
      <View style={{padding: 20}}>
        <P>{description}</P>
      </View>
    )}
  </View>
);

export default Panel;
