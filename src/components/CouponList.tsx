import React from 'react';
import {View, FlatList, ActivityIndicator, RefreshControl} from 'react-native';
import CouponItem from '@components/CouponItem';
import NoData from './NoData';
import {Col, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  isLoading: boolean;
  scrollEnabled?: boolean;
  coupons: any;
  onRefresh: Function;
  onEndReached: Function;
}

const CouponList = ({
  isLoading,
  scrollEnabled,
  coupons,
  onRefresh,
  onEndReached,
}: Props) => {
  return (
    <Col style={{flex: 1, paddingHorizontal: 10}}>
      {!isLoading && coupons.length == 0 && <NoData title="ไม่มีคูปอง" />}
      <FlatList
        scrollEnabled={scrollEnabled}
        data={coupons}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={() => onRefresh()}
            tintColor={colors.primaryColor}
          />
        }
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={1}
        numColumns={1}
        showsVerticalScrollIndicator={false}
        renderItem={({item, index}: {item: any; index: number}) => {
          return (
            <CouponItem
              key={item.id}
              id={item.id}
              image={item.photo}
              name={item.name}
              point={item.point}
              expire={item.expire}
              status={item.status}
            />
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          marginVertical: 10,
          paddingBottom: 10,
        }}
      />
    </Col>
  );
};

export default CouponList;
