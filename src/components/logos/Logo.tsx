import React from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import GreenLogo from '@images/logo_green/logo.png';
import GreyLogo from '@images/logo_grey/logo.png';

const Logo = ({ width, height, isGrey }) => (
  <Image
    style={{
      width,
      height,
    }}
    source={isGrey ? GreyLogo : GreenLogo}
    resizeMode="contain"
  />
);

Logo.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  isGrey: PropTypes.bool,
};

Logo.defaultProps = {
  width: 142,
  height: 183,
  isGrey: false,
};

export default Logo;
