import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { H2 } from '@components/Global';
import colors from '@utils/colors';
import fonts from '@utils/fonts';

const OutlinedButton = ({ text, onPress, small, grey }) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View style={{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 15,
      borderRadius: 50,
      borderColor: grey ? colors.grayBorder : colors.primaryColor,
      color: grey ? colors.fontColor : colors.primaryColor,
      borderWidth: 2,
      marginTop: 10,
      marginBottom: 10,
    }}>
      <H2 green>{text}</H2>
    </View>
  </TouchableWithoutFeedback>
);

export default OutlinedButton;
