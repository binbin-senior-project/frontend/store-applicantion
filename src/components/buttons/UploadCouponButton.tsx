import React from 'react';
import {TouchableWithoutFeedback, Image} from 'react-native';
import colors from '@utils/colors';
import {P, Row} from '@components/Global';

interface Props {
  image?: string;
  onPress: Function;
}

const UploadCouponButton = ({image, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      alignCenter
      center
      style={{
        width: '100%',
        height: 150,
        borderRadius: 5,
        backgroundColor: colors.inputBackgroundColor,
        marginTop: 20,
        marginBottom: 20,
      }}>
      {image ? (
        <Image
          source={{uri: image}}
          resizeMode="contain"
          style={{
            width: '100%',
            height: '100%',
          }}
        />
      ) : (
        <P green>รูปโปรโมท</P>
      )}
    </Row>
  </TouchableWithoutFeedback>
);

export default UploadCouponButton;
