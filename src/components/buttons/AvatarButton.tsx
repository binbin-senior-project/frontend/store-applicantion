import React from 'react';
import {Image, TouchableWithoutFeedback} from 'react-native';
import {Row} from '@components/Global';
import avatarDefault from '@images/avatar.png';

interface Props {
  avatar?: string;
  isLarge?: boolean;
  onPress: Function;
}

const AvatarButton = ({avatar, isLarge, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      alignCenter
      center
      style={[
        {
          backgroundColor: '#ffffff',
          borderRadius: 70,
          width: 35,
          height: 35,
          overflow: 'hidden',
        },
        isLarge && {
          width: 70,
          height: 70,
        },
      ]}>
      {avatar ? (
        <Image
          source={{uri: avatar}}
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 70,
          }}
        />
      ) : (
        <Image
          source={avatarDefault}
          style={{
            width: '100%',
            height: '100%',
          }}
        />
      )}
    </Row>
  </TouchableWithoutFeedback>
);

export default AvatarButton;
