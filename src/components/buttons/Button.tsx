import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';
import {H2} from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';

interface Props {
  text: string;
  showNextIcon?: boolean;
  isWhite?: boolean;
  onPress: Function;
}

const Button = ({text, showNextIcon, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 50,
        width: '100%',
        backgroundColor: colors.primaryColor,
        marginTop: 10,
        marginBottom: 10,
      }}>
      <H2 white>{text}</H2>
      {showNextIcon && (
        <Icon
          name="arrow-right"
          size={30}
          color={colors.whiteColor}
          style={{marginLeft: 10}}
        />
      )}
    </View>
  </TouchableWithoutFeedback>
);

export default Button;
