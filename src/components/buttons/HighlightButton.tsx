import React from 'react';
import { TouchableOpacity } from 'react-native';
import { P } from '@components/Global';

interface Props {
  text: string;
  onPress: Function;
}

const HighlightButton = ({ text, onPress }: Props) => (
  <TouchableOpacity onPress={onPress}>
    <P green>{text}</P>
  </TouchableOpacity>
);

export default HighlightButton;
