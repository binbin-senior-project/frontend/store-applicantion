import React from 'react';
import {TouchableWithoutFeedback, Image} from 'react-native';
import colors from '@utils/colors';
import {P, Row} from '@components/Global';

interface Props {
  avatar?: Object;
  onPress: Function;
}

const UploadButton = ({avatar, onPress}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      alignCenter
      center
      style={{
        width: 110,
        height: 110,
        borderRadius: 110,
        backgroundColor: colors.inputBackgroundColor,
      }}>
      {avatar ? (
        <Image
          source={{uri: avatar}}
          style={{
            width: '100%',
            height: '100%',
            borderRadius: 100,
          }}
        />
      ) : (
        <P green>รูปโปรไฟล์</P>
      )}
    </Row>
  </TouchableWithoutFeedback>
);

export default UploadButton;
