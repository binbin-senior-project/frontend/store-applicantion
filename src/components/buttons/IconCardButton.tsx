import React from 'react';
import { Text, TouchableWithoutFeedback, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Feather';
import fontStyle from '@utils/fonts';
import colors from '@utils/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    backgroundColor: colors.fourthColor,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    marginBottom: 10,
  },
  containerLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconLeft: {
    marginRight: 15,
  },
});

const IconCardButton = ({ icon, text, showChevron, onPress }) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <View style={styles.container}>
      <View style={styles.containerLeft}>
        <Icon
          name={icon}
          size={18}
          color={colors.fontColor}
          style={styles.iconLeft}
        />
        <Text style={fontStyle.h3}>
          {text}
        </Text>
      </View>
      {showChevron && (
        <Icon
          name="chevron-right"
          size={18}
          color={colors.fontColor}
        />
      )}
    </View>
  </TouchableWithoutFeedback>
);

IconCardButton.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default IconCardButton;
