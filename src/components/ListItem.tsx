import React from 'react';
import {TouchableWithoutFeedback} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {Row, Col, P} from '@components/Global';
import {View} from 'react-native';
import colors from '@utils/colors';

interface Props {
  icon?: string;
  title: string;
  onPress: Function;
}

const ListItem = ({icon, title, onPress}: Props) => {
  return (
    <TouchableWithoutFeedback onPress={() => onPress()}>
      <View
        style={{
          padding: 20,
          borderBottomWidth: 1,
          borderBottomColor: colors.grayBorder,
        }}>
        <Row between alignCenter>
          <Col>
            <Row alignCenter>
              {!!icon && (
                <Icon
                  name={icon}
                  size={18}
                  color={colors.fontColor}
                  style={{marginRight: 15}}
                />
              )}
              <P>{title}</P>
            </Row>
          </Col>
          <Col>
            <Icon name="chevron-right" size={18} color={colors.fontColor} />
          </Col>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ListItem;
