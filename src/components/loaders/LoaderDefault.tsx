import React from 'react';
import {View, ActivityIndicator, Modal} from 'react-native';
import colors from '@utils/colors';

interface LoaderdefaultProps {
  isLoading?: boolean;
}

const Loaderdefault = ({isLoading}: LoaderdefaultProps) => {
  if (isLoading) {
    return (
      <Modal animationType="none" transparent={true} visible={isLoading}>
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
            zIndex: 99,
            height: '100%',
          }}>
          <ActivityIndicator size="large" color={colors.primaryColor} />
        </View>
      </Modal>
    );
  }

  return <View />;
};

export default Loaderdefault;
