import React from 'react';
import {FlatList, RefreshControl} from 'react-native';
import CouponBoostItem from '@components/CouponBoostItem';
import NoData from '@components/NoData';
import {Col, P} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  isLoading: boolean;
  scrollEnabled?: boolean;
  coupons: any;
  onItemPress: Function;
  onRefresh: Function;
  onEndReached: Function;
}

const CouponBoostList = ({
  isLoading,
  scrollEnabled,
  coupons,
  onItemPress,
  onRefresh,
  onEndReached,
}: Props) => {
  return (
    <Col style={{flex: 1, paddingHorizontal: 10}}>
      {!isLoading && coupons.length == 0 && <NoData title="ไม่มีคูปอง" />}
      <FlatList
        scrollEnabled={scrollEnabled}
        data={coupons}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={() => onRefresh()}
            tintColor={colors.primaryColor}
          />
        }
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={1}
        numColumns={1}
        renderItem={({item, index}: {item: any; index: number}) => {
          return (
            <CouponBoostItem
              key={item.id}
              id={item.id}
              image={item.photo}
              name={item.name}
              point={item.point}
              expire={item.expire}
              status={item.status}
              storeName={item.storeName}
              checked={item.checked}
              onItemPress={onItemPress}
            />
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          marginVertical: 10,
          paddingBottom: 10,
        }}
      />
    </Col>
  );
};

export default CouponBoostList;
