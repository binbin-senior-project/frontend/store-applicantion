import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import Coin from '@components/Coin';
import colors from '@utils/colors';
import fonts from '@utils/fonts';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.fourthColor,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.05)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    marginVertical: 5,
  },
  containerselected: {
    backgroundColor: colors.primaryColor,
  },
  couponName: {
    ...fonts.paragraph,
    marginBottom: 5,
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
  },
  point: {
    marginRight: 5,
  },
  detail: {
    ...fonts.subparagraph,
  },
  textWhite: {
    color: "#ffffff",
  }
});

interface CouponSelectrops {
  name: string,
  point: string,
  expire: string,
  type: string,
  selected?: boolean,
  onPress: any
}

const CouponSelect = ({
  name,
  point,
  expire,
  type,
  selected,
  onPress
}: CouponSelectrops) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[
        styles.container,
        selected && styles.containerselected
      ]}>
        <Text style={[
          styles.couponName,
          selected && styles.textWhite
        ]}>
          {name}
        </Text>
        <View style={styles.row}>
          <View style={styles.column}>
            <Text style={[
              styles.detail,
              styles.point,
              selected && styles.textWhite
            ]}>
              {point}
            </Text>
            <Coin />
          </View>
          <View style={styles.column}>
            <Text style={[
              styles.detail,
              selected && styles.textWhite
            ]}>
              หมดอายุ: {expire}
            </Text>
          </View>
          <View style={styles.column}>
            <Text style={[
              styles.detail,
              selected && styles.textWhite
            ]}>
              ประเภท: ทั่วไป
            </Text>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

export default CouponSelect;