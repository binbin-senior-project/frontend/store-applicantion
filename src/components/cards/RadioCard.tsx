import React from 'react';
import {TouchableWithoutFeedback, View} from 'react-native';
import {H3, Row, Col} from '@components/Global';
import colors from '@utils/colors';

interface Props {
  name: string;
  textRight?: string;
  selected: boolean;
  onPress: Function;
}

const RadioCard = ({name, textRight, selected, onPress}: Props) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: colors.fourthColor,
          paddingHorizontal: 15,
          paddingVertical: 20,
          borderRadius: 5,
          shadowColor: 'rgba(0, 0, 0, 0.05)',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowRadius: 4,
          shadowOpacity: 1,
          marginVertical: 5,
        }}>
        <Col>
          <Row alignCenter>
            <View
              style={{
                backgroundColor: colors.backgroundColor,
                borderRadius: 20,
                width: 25,
                height: 25,
                marginRight: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              {selected && (
                <View
                  style={{
                    width: 15,
                    height: 15,
                    backgroundColor: colors.primaryColor,
                    borderRadius: 10,
                  }}
                />
              )}
            </View>
            <H3>{name}</H3>
          </Row>
        </Col>
        {textRight && (
          <Col>
            <H3 green>{textRight}</H3>
          </Col>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

export default RadioCard;
