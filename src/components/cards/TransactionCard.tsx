import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import fonts from '@utils/fonts';
import colors from '@utils/colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.inputBackgroundColor,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 5,
    marginVertical: 5,
  },
  containerLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconLeft: {
    marginRight: 15,
  },
});

interface Props {
  text: string;
  time: string;
}

const TransactionCard = ({text, time}: Props) => (
  <View style={styles.container}>
    <Text style={fonts.paragraph}>{text}</Text>
    <Text style={fonts.subparagraph}>เวลา {time}</Text>
  </View>
);

export default TransactionCard;
