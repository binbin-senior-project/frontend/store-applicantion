import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {H3, P, Row, Col} from '@components/Global';
import Coin from '@components/Coin';
import colors from '@utils/colors';
import fonts from '@utils/fonts';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.fourthColor,
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderRadius: 5,
    shadowColor: 'rgba(0, 0, 0, 0.05)',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 1,
    marginVertical: 5,
  },
});

interface CouponProps {
  name: string;
  point: string;
  expire: string;
  type: string;
  onPress: any;
}

const Coupon = ({name, point, expire, type, onPress}: CouponProps) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <H3>{name}</H3>
        <Row>
          <Row alignCenter style={{marginRight: 5}}>
            <P style={{marginRight: 5}}>{point}</P>
            <Coin />
          </Row>
          <Row alignCenter style={{marginRight: 5}}>
            <P>หมดอายุ: {expire}</P>
          </Row>
          <Row>
            <P>ประเภท: {type}</P>
          </Row>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Coupon;
