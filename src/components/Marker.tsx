import React from 'react';
import {View, Image} from 'react-native';
import colors from '@utils/colors';
import {Col} from './Global';

interface Props {
  source: any;
}

const Marker = ({source}: Props) => {
  return (
    <Col center>
      <View
        style={{
          width: 50,
          height: 50,
          borderRadius: 100,
          backgroundColor: colors.primaryColor,
          padding: 3,
        }}>
        <Image
          source={source}
          resizeMode="cover"
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: colors.whiteColor,
            borderRadius: 50,
          }}
        />
      </View>
      <View
        style={{
          width: 4,
          height: 15,
          backgroundColor: colors.primaryColor,
          borderRadius: 20,
          marginTop: -2,
        }}
      />
    </Col>
  );
};

export default Marker;
