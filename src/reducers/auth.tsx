import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_ERROR,
  USER_LOGOUT,
} from '../actions/auth';

const initialState = {
  accessToken: '',
  isLoggedIn: false,
  error: {message: ''},
};

function reducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return {...state};
    case USER_LOGIN_SUCCESS:
      return {...state, ...action.data, isLoggedIn: true};
    case USER_LOGIN_ERROR:
      return {...initialState, error: action.error};
    case USER_LOGOUT:
      return {...initialState};
    default:
      return state;
  }
}

export default reducer;
