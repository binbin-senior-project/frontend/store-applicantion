import {SET_CURRENT_STORE, RESET_CURRENT_STORE} from '../actions/store';

const initialState = {
  id: '',
  logo: '',
  name: '',
  tagline: '',
  phone: '',
  latitude: 0,
  longitude: 0,
  category: {},
};

function storeReducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case SET_CURRENT_STORE:
      return {...state, ...action.store};
    case RESET_CURRENT_STORE:
      return {...initialState};
    default:
      return state;
  }
}

export default storeReducer;
