import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import authReducer from './auth';
import registerReducer from './register';
import storeReducer from './store';
import notificationReducer from './notification';

const reducers = combineReducers({
  auth: authReducer,
  register: registerReducer,
  store: storeReducer,
  notification: notificationReducer,
});

export default createStore(reducers, applyMiddleware(thunk));
