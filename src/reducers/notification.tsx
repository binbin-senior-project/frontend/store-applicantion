import {
  SET_NEW_NOTIFICATION,
  RESET_NOTIFICATION,
} from '../actions/notification';

const initialState = {
  items: 0,
};

function storeReducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case SET_NEW_NOTIFICATION:
      return {...state, items: state.items + 1};
    case RESET_NOTIFICATION:
      return {...initialState};
    default:
      return state;
  }
}

export default storeReducer;
