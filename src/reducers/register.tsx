import {
  REGISTER_RESET,
  REGISTER_SET_EMAIL,
  REGISTER_SET_PASSWORD,
  REGISTER_SET_PASSCODE,
  REGISTER_SET_STORE_LOGO,
  REGISTER_SET_STORE_NAME,
  REGISTER_SET_STORE_TAGLINE,
  REGISTER_SET_STORE_PHONE,
  REGISTER_SET_STORE_CATEGORYID,
  REGISTER_SET_STORE_LATITUDE,
  REGISTER_SET_STORE_LONGITUDE,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_ERROR,
} from '../actions/register';

const initialState = {
  email: '',
  password: '',
  storeLogo: '',
  storeName: '',
  storeTagline: '',
  storePhone: '',
  categoryID: '',
  storeLatitude: '',
  storeLongitude: '',
  isRegistered: false,
  error: {
    message: '',
  },
};

function reducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case REGISTER_RESET:
      return {...initialState};
    case REGISTER_SET_EMAIL:
      return {...state, email: action.email};
    case REGISTER_SET_PASSWORD:
      return {...state, password: action.password};
    case REGISTER_SET_PASSCODE:
      return {...state, passcode: action.storePasscode};
    case REGISTER_SET_STORE_LOGO:
      return {...state, storeLogo: action.storeLogo};
    case REGISTER_SET_STORE_NAME:
      return {...state, storeName: action.storeName};
    case REGISTER_SET_STORE_TAGLINE:
      return {...state, storeTagline: action.storeTagline};
    case REGISTER_SET_STORE_PHONE:
      return {...state, storePhone: action.storePhone};
    case REGISTER_SET_STORE_CATEGORYID:
      return {...state, categoryID: action.categoryID};
    case REGISTER_SET_STORE_LATITUDE:
      return {...state, storeLatitude: action.storeLatitude};
    case REGISTER_SET_STORE_LONGITUDE:
      return {...state, storeLongitude: action.storeLongitude};
    case REGISTER_REQUEST:
      return {...state};
    case REGISTER_SUCCESS:
      return {...initialState, isRegistered: true};
    case REGISTER_ERROR:
      return {...state, error: action.error};
    default:
      return state;
  }
}

export default reducer;
