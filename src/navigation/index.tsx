import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AuthLoading from '@containers/AuthLoading';
import RegisterStack from '@containers/screens/register';
import Navigator from './navigator';

const Stack = createStackNavigator();

const Switcher = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Loading" headerMode="none">
        <Stack.Screen
          name="Loading"
          component={AuthLoading}
          options={{animationEnabled: false}}
        />
        <Stack.Screen
          name="RegisterStack"
          component={RegisterStack}
          options={{animationEnabled: false}}
        />
        <Stack.Screen
          name="Main"
          component={Navigator}
          options={{animationEnabled: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Switcher;
