import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import messaging from '@react-native-firebase/messaging';
import Icon from 'react-native-vector-icons/Feather';
import HomeStack from '@containers/screens/home';
import BoostStack from '@containers/screens/boost';
import NotificationStack from '@containers/screens/notification';
import ProfileStack from '@containers/screens/profile';
import colors from '@utils/colors';
import {setNewNotifcationAction} from '@actions/notification';

const Tab = createBottomTabNavigator();

interface Props {
  notification: any;
  setNewNotification: Function;
  navigation: any;
}

const Navigator = ({notification, setNewNotification, navigation}: Props) => {
  useEffect(() => {
    const unsubscribe = messaging().onMessage(() => {
      setNewNotification();
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: colors.primaryColor,
        inactiveTintColor: colors.fontColor,
        style: {
          height: 100,
          paddingTop: 15,
          borderTopColor: colors.iconColor,
          alignItems: 'center',
        },
        labelStyle: {
          fontFamily: 'DBHeavent',
          fontSize: 20,
          fontWeight: 'normal',
          fontStyle: 'normal',
          letterSpacing: 0,
        },
        allowFontScaling: true,
      }}>
      <Tab.Screen
        name="Home"
        component={HomeStack}
        options={{
          tabBarLabel: 'หน้าแรก',
          tabBarIcon: ({color, size}) => (
            <Icon name="home" size={25} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Boost"
        component={BoostStack}
        options={{
          tabBarLabel: 'บูสต์คูปอง',
          tabBarIcon: ({color, size}) => (
            <Icon name="gift" size={25} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Notification"
        component={NotificationStack}
        options={{
          tabBarLabel: 'แจ้งเตือน',
          tabBarIcon: ({color, size}) => (
            <View
              style={{
                position: 'relative',
              }}>
              {!!notification.items && (
                <View
                  style={{
                    position: 'absolute',
                    top: -5,
                    right: -5,
                    width: 20,
                    height: 20,
                    borderRadius: 20,
                    backgroundColor: colors.redColor,
                    alignItems: 'center',
                    justifyContent: 'center',
                    zIndex: 2,
                  }}>
                  <Text style={{color: colors.whiteColor, textAlign: 'center'}}>
                    {notification.items}
                  </Text>
                </View>
              )}
              <Icon name="bell" size={25} color={color} />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarLabel: 'โปรไฟล์',
          tabBarIcon: ({color, size}) => (
            <Icon name="user" size={25} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const mapStateToProps = (state: any) => ({
  notification: state.notification,
});

const mapDispactToProps = (dispatch: Function) => ({
  setNewNotification: () => dispatch(setNewNotifcationAction()),
});

export default connect(
  mapStateToProps,
  mapDispactToProps,
)(Navigator);
