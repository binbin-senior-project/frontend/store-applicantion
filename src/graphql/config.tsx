import ApolloClient from 'apollo-boost';

export const client = new ApolloClient({
  // uri: 'http://172.20.10.2:7002/query',
  // uri: 'http://localhost:7002/query',
  uri: 'http://store.binbindev.com/query',
});
