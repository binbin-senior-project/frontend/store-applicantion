import gql from 'graphql-tag';

export const REQUEST_CREATE_STORE = gql`
  mutation CreateStore($input: NewStore!) {
    createStore(input: $input) {
      success
    }
  }
`;

export const REQUEST_CREATE_COUPON = gql`
  mutation CreateCoupon($input: NewCoupon!) {
    createCoupon(input: $input) {
      success
    }
  }
`;

export const REQUEST_COUPON_BOOST = gql`
  mutation CrateCouponBoost($input: CouponBoostInput!) {
    requestCouponBoost(input: $input) {
      success
    }
  }
`;

export const SET_TOKEN_MESSAGING = gql`
  mutation SetTokenMessaging($input: TokenMessagingInput!) {
    setTokenMessaging(input: $input) {
      success
    }
  }
`;

export const UPDATE_COUPON = gql`
  mutation UpdateCoupon($input: UpdateCouponInput!) {
    updateCoupon(input: $input) {
      success
    }
  }
`;

export const UPDATE_STORE = gql`
  mutation UpdateStore($input: UpdateStoreInput!) {
    updateStore(input: $input) {
      success
    }
  }
`;
