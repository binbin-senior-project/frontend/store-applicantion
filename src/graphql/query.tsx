import gql from 'graphql-tag';

export const GET_STORES = gql`
  query getStores($offset: Int!, $limit: Int!) {
    getStores(offset: $offset, limit: $limit) {
      id
      logo
      name
      tagline
      phone
      latitude
      longitude
      category {
        id
        name
      }
    }
  }
`;

export const GET_STORE = gql`
  query getStore($storeID: String!) {
    getStore(storeID: $storeID) {
      id
      logo
      name
      tagline
      phone
      latitude
      longitude
      category {
        id
        name
      }
    }
  }
`;

export const GET_COUPONS = gql`
  query GetCoupons($storeID: String!, $offset: Int!, $limit: Int!) {
    getCoupons(storeID: $storeID, offset: $offset, limit: $limit) {
      id
      name
      photo
      point
      expire
      status
    }
  }
`;

export const GET_APPROVE_COUPONS = gql`
  query GetApproveCoupons($storeID: String!, $offset: Int!, $limit: Int!) {
    getApproveCoupons(storeID: $storeID, offset: $offset, limit: $limit) {
      id
      name
      photo
      point
      expire
      status
    }
  }
`;

export const GET_COUPON = gql`
  query GetCoupon($couponID: String!) {
    getCoupon(couponID: $couponID) {
      id
      name
      description
      photo
      condition
      point
      expire
      status
      redeems
      category {
        id
        name
      }
    }
  }
`;

export const GET_COUPON_CATEGORIES = gql`
  query GetCouponCategories($offset: Int!, $limit: Int!) {
    getCouponCategories(offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

export const GET_STORE_CATEGORIES = gql`
  query GetStoreCategories($offset: Int!, $limit: Int!) {
    getStoreCategories(offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

export const GET_COUPON_PRODUCTS = gql`
  query GetCouponProducts($offset: Int!, $limit: Int!) {
    getCouponProducts(offset: $offset, limit: $limit) {
      id
      name
      price
      day
    }
  }
`;

export const GET_TRANSACTIONS = gql`
  query GetTransactions($storeID: String!, $offset: Int!, $limit: Int!) {
    getTransactions(storeID: $storeID, offset: $offset, limit: $limit) {
      id
      description
      createdAt
    }
  }
`;
