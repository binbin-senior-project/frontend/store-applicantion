import React from 'react';
import {Provider} from 'react-redux';
import {ApolloProvider} from '@apollo/react-hooks';
import {client} from '@graphql/config';
import store from './src/reducers';
import Switcher from './src/navigation';

// Set Default Thai Time
import 'moment/locale/th';

console.disableYellowBox = true;

export default () => (
  <ApolloProvider client={client}>
    <Provider store={store}>
      <Switcher />
    </Provider>
  </ApolloProvider>
);
